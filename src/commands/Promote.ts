import { ApplicationCommandOptionType, ApplicationCommandType, CommandInteraction, GuildMember, User, UserContextMenuCommandInteraction, userMention } from "discord.js";
import { UserModel } from "../database/models/User"

import PERMISSIONS, { NeedsPermission } from "../permissions/Permissions";
import { ContextMenu, Discord, Guard, Guild, Slash, SlashChoice, SlashOption } from "discordx";
import { ENV } from "../Environment";
import { EmbedService } from "../embeds/embed.service";

function getPromoteMessage(username: string, grade: string) {
    return {
        "content": `\`${username}\` a maintenant le grade \`${grade}\``,
        "ephemeral": true
    }
}

@Discord()
@Guard(NeedsPermission("OWNER"))
@Guild(ENV.MAIN_GUILD)
export class PromoteCommand {

    @Slash({
        "name": "promote",
        "description": "Permet de modifier le grade d'un utilisateur",
        "dmPermission": false,
    })
    async promoteCommand(
        @SlashOption({
            "name": "utilisateur",
            "description": "Utilisateur à promouvoir",
            "required": true,
            "type": ApplicationCommandOptionType.User
        })
        user: GuildMember,
        @SlashChoice(...Object.values(PERMISSIONS))
        @SlashOption({
            "name": "grade",
            "description": "Grade à attribuer",
            "required": true,
            "type": ApplicationCommandOptionType.Number,
            "minValue": 1,
            "maxValue": 5
        })
        grade: 1 | 2 | 3 | 4 | 5,
        interaction: CommandInteraction
    ) {
        promote(user, grade);
        this.sendPromoteMessage(interaction, user.id, Object.keys(PERMISSIONS)[grade - 1]);
    }

    @ContextMenu({
        "type": ApplicationCommandType.User,
        "name": "Promotion - UTILISATEUR"
    })
    async contextUtilisateurUtilisateur(interaction: UserContextMenuCommandInteraction) {
        promote(interaction.targetUser, 1);
        this.sendPromoteMessage(interaction, interaction.targetUser.id, "UTILISATEUR");
    }

    @ContextMenu({
        "type": ApplicationCommandType.User,
        "name": "Promotion - VIP"
    })
    async contextUtilisateurVIP(interaction: UserContextMenuCommandInteraction) {
        promote(interaction.targetUser, 2);
        this.sendPromoteMessage(interaction, interaction.targetUser.id, "VIP");
    }

    async sendPromoteMessage(interaction: CommandInteraction | UserContextMenuCommandInteraction, userID: string, grade: string) {
        await interaction.deferReply()
        const embed = EmbedService.buildSuccess({
            "title": "Promotion",
            "description": `L'utilisateur ${userMention(userID)} a maintenant le grade \`${grade}\``
        })
        interaction.editReply({
            "embeds": [embed]
        })
    }

    @ContextMenu({
        "type": ApplicationCommandType.User,
        "name": "Promotion - MODERATEUR"
    })
    async contextUtilisateurModerateur(interaction: UserContextMenuCommandInteraction) {
        promote(interaction.targetUser, 3);
        this.sendPromoteMessage(interaction, interaction.targetUser.id, "MODERATEUR");
    }

    @ContextMenu({
        "type": ApplicationCommandType.User,
        "name": "Promotion - ADMINISTRATEUR"
    })
    async contextUtilisateurAdministrateur(interaction: UserContextMenuCommandInteraction) {
        promote(interaction.targetUser, 4);
        this.sendPromoteMessage(interaction, interaction.targetUser.id, "ADMINISTRATEUR");
    }
}

async function promote(user: User | GuildMember, grade: 1 | 2 | 3 | 4 | 5) {
    const userDB = new UserModel();
    userDB.id = user.id;
    userDB.role = grade;
    await UserModel.save(userDB)
}

export default promote