import ArrayUtil from "blessed-api/dist/utils/ArrayUtils";
import { getQuizQuestionEmbed } from "../QuizStart";
import { Question } from "../questions/Question";
import { ItemsService } from "../../items/items.service";
import H4AItems from "../../../core/api/hamm4all/items-api";

const imageSize = 50

async function askCoeff(): Promise<Question> {
    const item = await ItemsService.getRandomItem();
    const image = await ItemsService.getImage(item, imageSize);

    const embed = getQuizQuestionEmbed()
        .addFields([
            {
                "name": "Question",
                "value": "Quel est le **coefficient** de cet objet ?"
            }
        ])
        .setImage('attachment://image.png');

    return {
        "question": embed,
        "attachedFiles": [{name: "image.png", attachment: image || Buffer.from("")}],
        "answers": {
            "anwsers": ["" + item.coefficient],
            "multiplier": 1 + (0.5 / (item.coefficient <= 0 ? 6 : item.coefficient))
        }
    }
}

async function askName(): Promise<Question> {
    const item = await ItemsService.getRandomItem();
    const image = await ItemsService.getImage(item, imageSize)

    const embed = getQuizQuestionEmbed()
        .addFields([
            {
                "name": "Question",
                "value": "Quel est le **nom** de cet objet ?"
            }
        ])
        .setImage('attachment://image.png');

    return {
        "question": embed,
        "attachedFiles": [{name: "image.png", attachment: image || Buffer.from("")}],
        "answers": {
            "anwsers": Object.values(item.name).map(res => res.trim()),
            "multiplier": 1
        }
    }
}

async function askPoints(): Promise<Question> {
    const items = await H4AItems.getItemsByCondition(it => typeof it.value === "number");

    const item = ArrayUtil.randomElement(items)
    const image = await ItemsService.getImage(item, imageSize);

    const embed = getQuizQuestionEmbed()
        .addFields([
            {
                'name': "Question",
                "value": "Quel est la **valeur** de cet objet ?"
            }
        ])
        .setImage('attachment://image.png');

    return {
        "question": embed,
        "attachedFiles": [{name: "image.png", attachment: image || Buffer.from("")}],
        "answers": {
            "anwsers": ["" + item.value],
            "multiplier": 1
        }
    }
}

export default {
    askName, askCoeff, askPoints
}