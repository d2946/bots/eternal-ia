import { H4AGamesService } from "../../games/games.hamm4all.service";
import { Question } from "../questions/Question";
import { getQuizQuestionEmbed } from "../QuizStart";
import H4AFiles from "../../../core/api/hamm4all/files-api";
import { Game } from "@hamm4all/shared";
import H4AGames from "../../../core/api/hamm4all/games-api";

export async function askGameName(): Promise<Question> {
    const game = await H4AGamesService.getRandomGame({needsToHaveThumbnail: true});
    const image = await H4AFiles.getResizedFile(game.thumbnail_id, 420, 500)

    const embed = getQuizQuestionEmbed()
        .addFields([
            {
                'name': "Question",
                "value": "Quel est le **nom** de ce jeu ?"
            }
        ])
        .setImage("attachment://image.png");

    return {
        "question": embed,
        "answers": {
            "anwsers": [game.name],
            "multiplier": 1 + (game.name.length - 6) / 25
        },
        "attachedFiles": [{name: "image.png", attachment: image}]
    };
}

export async function getRandomLevel(games: Array<Game>, needsToHaveScreens: boolean = true) {

    const gameIndex = Math.floor(Math.random() * games.length);
    const level = await H4AGamesService.getRandomLevel(games[gameIndex].id, {needsToHaveScreens: needsToHaveScreens});

    if (!level) return getRandomLevel(games, needsToHaveScreens)

    return {
        game: games[gameIndex],
        level
    }
}

export async function getRandomLevelWithScreen(games: Array<Game>) {
    let {game, level} = await getRandomLevel(games, true)

    return {
        game,
        level
    }
}

export async function askLevelName(): Promise<Question> {
    const lands = await H4AGames.getGames();
    const {game, level} = await getRandomLevelWithScreen(lands)

    const image = await H4AFiles.getResizedFile(level.mainScreenshotID!, 420, 500)
    const embed = getQuizQuestionEmbed()
        .addFields([
            {
                "name": "Question",
                "value": "Quel est le **numéro** de ce niveau ?"
            }
        ])
        .setImage("attachment://image.png");

    const qu: Question = {
        "question": embed,
        "answers": {
            "anwsers": [level.name],
            "multiplier": calculateMultiplier()
        },
        "attachedFiles": [{name: "image.png", attachment: image}]
    }

    return qu;

    function calculateMultiplier(): number {
        let total = 1;
        total += (game.level_count / 250)
        const l = level
        const levelLength = l.name.length
        const points = (l.name.match(/\./g) || []).length;
        const semicolons = (l.name.match(/;/g) || []).length;
        total *= 1 + (levelLength - 10) / 25;
        total += points * 0.12;
        total += semicolons * 0.3;
        return total
    }
}


export async function askGameNameFromLevel(): Promise<Question> {
    const lands = await H4AGames.getGames();
    const {game, level} = await getRandomLevelWithScreen(lands)

    const image = await H4AFiles.getResizedFile(level.mainScreenshotID!, 420, 500)
    const embed = getQuizQuestionEmbed()
        .addFields([
            {
                "name": "Question",
                "value": "Quel est le **nom** du **jeu** de ce niveau ?"
            }
        ])
        .setImage('attachment://image.png');

    return {
        "question": embed,
        "answers": {
            'anwsers': [game.name],
            "multiplier": 1 + (game.name.length - 8) / 30 
        },
        "attachedFiles": [{name: "image.png", attachment: image}]
    };
}

export default {
    askGameName,
    askLevelName,
    askGameNameFromLevel,
    getRandomLevelWithScreen
}