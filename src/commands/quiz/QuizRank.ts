import { buildTable } from "../../modules/Display";
import connection from "../../database/Connection";
import { getTitle } from "./QuizRecap";
import { Discord, Guild as GuildX, Slash, SlashGroup } from "discordx";
import { ENV } from "../../Environment";
import { CommandInteraction, Guild } from "discord.js";
import UnknownError from "../../errors/UnknownError";
import BlessedAPI from "blessed-api";
import { PaginationService } from "../../modules/pagination/pagination.service";

type OutputDatabase = Array<{winner: string, points: number}>

@Discord()
@SlashGroup("quiz")
@GuildX(ENV.MAIN_GUILD)
export class QuizRankCommand {

    @Slash({
        "name": "rank",
        "description": "Montre le classement de points de l'Eternal Quiz du serveur",
        "dmPermission": false
    })
    async rank(
        interaction: CommandInteraction
    ) {
        await interaction.deferReply();

        const guild = interaction.guild;
        if (! guild) {
            throw new UnknownError();
        }

        const registered: OutputDatabase = await connection.query(`
            SELECT
                winner,
                SUM(reward) AS points
            FROM
                quiz
            WHERE
                winner IS NOT NULL
            GROUP BY
                winner
            ORDER BY
                points DESC`
        );

        const table = buildTable({
            "title": "Classement de l'Eternal Quiz",
            "headers": ["Position", "Joueur", "Titre", "Points"],
            "data": registered.map((member, i) => {
                const name = guild.members.cache.get(member.winner)?.displayName ?? guild.client.users.cache.get(member.winner)?.username ?? "Joueur inconnu"

                return [
                    (i + 1).toString(),
                    name,
                    getTitle(member.points).title.name,
                    BlessedAPI.Utils.Number.roundDecimal(member.points, 2).toLocaleString()
                ]
            })
        })

        const pagination = await PaginationService.addPagination({
            "messageId": interaction.id,
            "pagination": {
                "currentPageIndex": 0,
                "pages": table.map((page) => {
                    return {
                        "content": page,
                    }
                })
            }
        })

        await interaction.editReply(pagination);
    }
}

function buildResult(guild: Guild, registered: OutputDatabase, page: number) {
    const data: Array<Array<string>> = [];

    let i = 0;
    for (const member of registered) {
        
        i++
        const name = guild.members.cache.get(member.winner)?.displayName || guild.client.users.cache.get(member.winner)?.username
        if (! name) {
            continue;
        }

        data.push(["" + i, name, getTitle(member.points).title.name, BlessedAPI.Utils.Number.roundDecimal(member.points, 2).toLocaleString()])
    }

    return buildTable({
        "title": "Classement de l'Eternal Quiz",
        "headers": ["Position", "Joueur", "Titre", "Points"],
        "data": data,
    });

}
