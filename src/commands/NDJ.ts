import datasource from "../database/Connection";
import { NDJPing } from "../database/models/NDJPing";
import { ActionRowBuilder, ApplicationCommandOptionType, ButtonBuilder, ButtonInteraction, ButtonStyle, Client, Colors, CommandInteraction, EmbedBuilder } from "discord.js";
import { ButtonComponent, Discord, Slash, SlashGroup, SlashOption } from "discordx";
import { EmbedService } from "../embeds/embed.service";
import { PaginationService } from "../modules/pagination/pagination.service";

@Discord()
@SlashGroup({
    "name": "ndjping",
    "description": "Commandes liées au NDJ"
})
@SlashGroup("ndjping")
export class NDJCommand {

    @Slash({
        "name": "list",
        "description": "Liste les heures auxquelles sera pingé l'utilisateur"
    })
    async list(interaction: CommandInteraction) {
        await interaction.deferReply();

        const pings = await NDJPing.find({
            "where": {
                "id_user": interaction.user.id
            },
            "order": {
                "ping_time": "ASC"
            }
        });

        const embeds: Array<EmbedBuilder> = [];

        const PINGS_PER_PAGE = 10
        for(let i = 0 ; i < pings.length ; i += PINGS_PER_PAGE) {
            embeds.push(await this.getPingEmbed(interaction, pings.slice(i, i + PINGS_PER_PAGE)));
        }

        if (embeds.length === 0) {
            embeds.push(await this.getPingEmbed(interaction, []));
        }

        const pagination = await PaginationService.addPagination({
            "messageId": interaction.id,
            "pagination": {
                "currentPageIndex": 0,
                "pages": embeds.map(e => ({"embeds": [e]}))
            }
        });

        await interaction.editReply(pagination);
    }

    @Slash({
        "name": "register",
        "description": "Enregistre une heure à laquelle tu veux être pingé"
    })
    async register(
        @SlashOption({
            "name": "heure",
            "description": "Heure à laquelle tu veux être pingé",
            "required": true,
            "type": ApplicationCommandOptionType.Integer,
            "minValue": 0,
            "maxValue": 23
        })
        hours: number,
        @SlashOption({
            "name": "minute",
            "description": "Minute à laquelle tu veux être pingé",
            "required": true,
            "type": ApplicationCommandOptionType.Integer,
            "minValue": 0,
            "maxValue": 59
        })
        minutes: number,
        interaction: CommandInteraction
    ) {
        await interaction.deferReply();


        let ndjPing = await NDJPing.findOne({
            "where": {
                "id_user": interaction.user.id,
                "ping_time": hours * 60 + minutes
            }
        });
        let created = false;

        if (! ndjPing) {
            ndjPing = new NDJPing();
            ndjPing.id_user = interaction.user.id;
            ndjPing.ping_time = hours * 60 + minutes;
            NDJPing.save(ndjPing);
            created = true;
        }

        if (created) {
            await interaction.editReply({
                "embeds": [EmbedService.buildSuccess({
                    "title": "NDJ Ping",
                    "description": `Ton ping a bien été enregistré à ${ndjPing.getTime()}`
                })]
            });
        }
        // Remove the ping
        else {
            await NDJPing.remove(ndjPing);
            await interaction.editReply({
                "embeds": [EmbedService.buildSuccess({
                    "title": "NDJ Ping",
                    "description": `Ton ping à ${ndjPing.getTime()} a bien été supprimé`
                })]
            });
        }

    }

    private async getPingEmbed(interaction: CommandInteraction, pings: NDJPing[]) {
        const embed = new EmbedBuilder()
            .setTitle("NDJ Ping")
            .setDescription("Liste des heures auxquelles tu seras pingé")
            .setTimestamp()
            .setAuthor({
                "name": interaction.user.username,
                "iconURL": interaction.user.avatarURL() ?? undefined
            })

        if (pings.length === 0) {
            embed.addFields([{
                "name": "Aucun ping",
                "value": "Tu n'as aucun ping de configuré"
            }])
        }
        else {
            embed.addFields([
                {
                    "name": "Heure",
                    "value": pings.map(p => p.getTime()).join("\n"),
                }
            ])
        }

        return embed;
    }

    @ButtonComponent({ "id": "ndjping-done" })
    async done(interaction: ButtonInteraction) {
        const user = interaction.user;

        // Validate all NDJPings of the user
        await datasource.createQueryBuilder()
            .update(NDJPing)
            .set({
                "done": true
            })
            .where("id_user = :id_user", { "id_user": user.id })
            .execute();

        const success = EmbedService.buildSuccess({
            "title": "NDJ Ping",
            "description": "Bravo pour avoir joué au NDJ aujourd'hui !"
        })

        await interaction.update({
            "embeds": [success],
            "components": [] // Remove buttons
        })
    }

    private static pingActivated: boolean = false;

    static activatePings(client: Client) {
        if (this.pingActivated) return; // Already activated
        setInterval(async () => {
            const now = new Date();
            const hours = now.getHours();
            const minutes = now.getMinutes();
            const time = hours * 60 + minutes;
    
            const pings = await NDJPing.getAllPingsAtTime(time);
    
            for(const ping of pings) {
                if (ping.done) continue;
                const user = await client.users.fetch(ping.id_user);
                const embed = new EmbedBuilder()
                    .setTitle("NDJ Ping")
                    .setDescription("C'est l'heure de jouer au NDJ !")
                    .setColor(Colors.Yellow)
                    .setTimestamp()
    
                const button = new ButtonBuilder()
                    .setStyle(ButtonStyle.Success)
                    .setLabel("J'ai joué")
                    .setCustomId("ndjping-done")
    
                const actionRow = new ActionRowBuilder<ButtonBuilder>()
                    .addComponents([button])
    
                await user.send({
                    "embeds": [embed],
                    "components": [actionRow]
                })
            }
        }, 1000 * 60 /* 1 minute */)
        this.pingActivated = true;
    }

}