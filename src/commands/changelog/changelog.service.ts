import { EmbedBuilder, bold, underline } from "discord.js";
import changes from "./changes/changelog.changes";
import { BotException } from "../../errors/bot.exception";
import { EMPTY_CHARACTER } from "../../commands/markdown/markdown.service";

export class ChangelogService {

    /**
     * Check if there is at least one changelog
     */
    checkChangelog() {
        if (changes.length === 0) {
            throw new BotException("Aucun Changelog n'a été trouvé");
        }
    }

    getChangelogByIndex(index: number): EmbedBuilder {
        const realIndex = Math.max(Math.min(index, changes.length - 1), 0);
        const version = changes[realIndex];
        const grayLevel = Math.floor(realIndex / changes.length * 256);
        const res = new EmbedBuilder()
            .setTitle(`Version ${version.version}`)
            .setColor([
                grayLevel,
                grayLevel,
                grayLevel
            ])

        this.addNewFeatures(res, version);
        this.addImprovements(res, version);
        this.addFixes(res, version);

        if (version.updatedAt) {
            res.setFooter({
                "text": `Mis à jour le ${version.updatedAt.toLocaleDateString("fr-FR", {
                    "day": "numeric",
                    "month": "long",
                    "year": "numeric"
                })}`
            })
        }

        return res;
    }

    addNewFeatures(embed: EmbedBuilder, change: Change): void {
        this.addChangesField(embed, bold("Nouvelles fonctionnalités"), change.changes.feature);
    }

    addImprovements(embed: EmbedBuilder, change: Change): void {
        this.addChangesField(embed, "Améliorations", change.changes.upgrade);
    }

    addFixes(embed: EmbedBuilder, change: Change): void {
        this.addChangesField(embed, "Bugfixes ou équilibrages", change.changes.fix);
    }

    addChangesField(embed: EmbedBuilder, title: string, categoryChanges?: Array<string>): void {
        if (categoryChanges && categoryChanges.length > 0) {
            embed.addFields([
                {
                    "name": underline(bold(title.toUpperCase())),
                    "value": `- ${categoryChanges.join("\n- ")}\n\n${EMPTY_CHARACTER}`,
                }
            ])
        }
    }

}