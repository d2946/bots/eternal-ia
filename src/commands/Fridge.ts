import { HammerfestUser } from "blessed-api/dist/eternaltwin/v1/types/eternaltwin.hammerfest.types";
import { EternalTwinUser } from "blessed-api/dist/eternaltwin/v1/types/eternaltwin.users.types";
import Users from "../cache/Users";
import { eternaltwinHammerfestUserCache, eternaltwinUsersCache } from "../cache/eternaltwin.cache";
import H4AItems from "../core/api/hamm4all/items-api";
import { ApplicationCommandOptionType, Colors, CommandInteraction, EmbedBuilder } from "discord.js";
import { Discord, Slash, SlashChoice, SlashOption } from "discordx";
import BotError from "../errors/BotError";

export type HammerfestServerWithUnderscore = "hammerfest_fr" | "hammerfest_es" | "hfest_net"
export type HammerfestServerWithDot = "hammerfest.fr" | "hammerfest.es" | "hfest.net"
export const hammerfestServers = ["hammerfest_fr", "hammerfest_es", "hfest_net"] as const;
export const converter: Record<HammerfestServerWithUnderscore, HammerfestServerWithDot> = {
    "hammerfest_fr": "hammerfest.fr",
    "hammerfest_es": "hammerfest.es",
    "hfest_net": "hfest.net"
}

@Discord()
export class Fridge {
    @Slash({
        "name": "fridge",
        "description": "Donne des statistiques sur le frigo en question"
    })
    async fridge(
        @SlashOption({
            "name": "username",
            "description": "Nom de l'utilisateur",
            "required": true,
            "type": ApplicationCommandOptionType.String,
            "autocomplete": async (interaction) => {
                const input = interaction.options.getFocused().toString()
                const users = await Users.autocomplete(input, "id");
                await interaction.respond(users);
            }
        })
        eternaltwinUserID: string,
        @SlashChoice(...hammerfestServers)
        @SlashOption({
            "name": "server",
            "description": "Nom du serveur",
            "required": true,
            "type": ApplicationCommandOptionType.String
        })
        server: HammerfestServerWithUnderscore,
        @SlashOption({
            "name": "display-coeff-0",
            "description": "Afficher les coefficients 0",
            "required": false,
            "type": ApplicationCommandOptionType.Boolean
        })
        displayCoeff0: boolean,
        interaction: CommandInteraction
    ) {
        await interaction.deferReply();
        displayCoeff0 ??= false;

        const eternalTwinUser = await this.getEternalTwinUser(eternaltwinUserID);
        const hammerfestUserID = this.getHammerfestUserID(eternalTwinUser, server)
        const hammerfestPlayer = await this.getHammerfestPlayer(eternalTwinUser, hammerfestUserID, server)

        const counts = await this.countItems(hammerfestPlayer, displayCoeff0)
        const embed = this.buildEmbed(eternalTwinUser, server, counts, displayCoeff0);

        await interaction.editReply({
            "embeds": [embed],
        })
    }

    buildEmbed(eternalTwinUser: EternalTwinUser, server: HammerfestServerWithUnderscore, itemCount: Array<number>, displayCoeff0: boolean) {
        return new EmbedBuilder()
            .setTitle(`Frigo de ${eternalTwinUser.display_name.current.value} (serveur ${converter[server]})`)
            .setFooter({
                "text": `ID: ${eternalTwinUser.id}`
            })
            .setColor(Colors.Yellow)
            .addFields(itemCount.map((count, index) => {
                const value = `${count} (**${(count / itemCount.reduce((a, b) => a + b, 0) * 100).toFixed(3)}%**)`;
                return {
                    "name": `Coeff ${index + (displayCoeff0 ? 0 : 1)}`,
                    "value": value,
                }
            }))
            .setTimestamp()
    }

    async countItems(hammerfestPlayer: HammerfestUser, displayCoeff0: boolean) {
        const allItems = await H4AItems.getItems()
            .then(items => items.filter(e => e.coefficient !== 0 || displayCoeff0));
        const fridge = hammerfestPlayer.inventory.latest.value

        const counts: Array<number> = [0, 0, 0, 0, 0, 0, 0, 0];
        for(let [id, count] of Object.entries(fridge)) {
            const itemData = allItems.find(e => e.in_game_id === parseInt(id));
            if (! itemData) {
                continue;
            }
            counts[itemData.coefficient] += count;
        }
        if (! displayCoeff0) counts.shift();
        return counts;
    }

    async getHammerfestPlayer(eternalTwinUser: EternalTwinUser, hammerfestUserID: string, server: HammerfestServerWithUnderscore) {
        const hammerfestPlayer = await eternaltwinHammerfestUserCache.getHammerfestUser(hammerfestUserID, converter[server]);
        if (! hammerfestPlayer) {
            throw new BotError(`Le joueur \`${eternalTwinUser.display_name.current.value}\` n'est pas sur le serveur \`${server}\``)
        }
        return hammerfestPlayer;
    }

    getHammerfestUserID(user: EternalTwinUser, server: HammerfestServerWithUnderscore): string {
        const hammerfestServer = user.links[server];
        if (! hammerfestServer?.current) {
            throw new BotError(`Le joueur ${user.display_name.current.value} n'est pas sur le serveur ${server}`)
        }
        return hammerfestServer.current.user.id;
    }

    async getEternalTwinUser(eternaltwinUserID: string): Promise<EternalTwinUser> {
        const user = await eternaltwinUsersCache.getUser(eternaltwinUserID)
            .then(user => user)
            .catch(() => {
                throw new BotError(`Je n'ai pas trouvé de joueur avec l'ID \`${eternaltwinUserID}\``)
            }
        );
        if (! user) {
            throw new BotError(`Je n'ai pas trouvé de joueur avec l'ID \`${eternaltwinUserID}\``)
        }
        return user;
    }

}