import { ETERNALFEST_FILES_URL } from "../../../Const";
import { Game } from "blessed-api/dist/eternalfest/v1/types/eternalfest.game.types";
import { Music } from "blessed-api/dist/eternalfest/v1/types/eternalfest.types";

export class MusicsService {
    async getAllRealMusics(game: Game) {
        return game.channels.active.build.musics
            .filter(resource => !resource.display_name.endsWith("silence.mp3"))
    }

    async formatMusicsToMarkdownLink(musics: Array<Music>) {
        const formattedMusics = musics.map((music, index) => {
            const URL = `${ETERNALFEST_FILES_URL}/${music.blob.id}/raw`
            const name = music.display_name.split("/").pop();
            return `${index + 1}. [${name}](${URL})`
        })

        const splits = [];
        for(let i = 0 ; i < formattedMusics.length ; i += 5) {
            splits.push(formattedMusics.slice(i, i + 5))
        }

        return splits.map((split) => ({
            "name": "Musiques",
            "value": split.join("\n"),
            "inline": false
        }))
    }
}