import { Level } from "@hamm4all/shared";
import BlessedAPI from "blessed-api";
import H4AGames from "../../core/api/hamm4all/games-api";
import H4ALevels from "../../core/api/hamm4all/levels-api";

export class H4AGamesService {
    
    static async getRandomGame({needsToHaveThumbnail} = {needsToHaveThumbnail: false}) {
        let games = await H4AGames.getGames();
        if (needsToHaveThumbnail) {
            games = games.filter((game) => game.thumbnail_id !== undefined);
        }

        return BlessedAPI.Utils.Array.randomElement(games);
    }

    static async getRandomLevel(gameID: number, {needsToHaveScreens} = {needsToHaveScreens: false}) {
        let levels = await this.getLevelsFromGame(gameID, needsToHaveScreens);
        if ((! levels) || levels.length === 0) {
            return undefined;
        }

        const chosenLevel = BlessedAPI.Utils.Array.randomElement(levels);

        const level = await H4ALevels.getLevel(gameID, chosenLevel.name);
        return level
    }

    private static async getLevelsFromGame(gameId: number, needsToHaveScreens: boolean) {
        const game = await H4AGames.getGame(gameId);

        if (! game) {
            return undefined;
        }

        let levels = await H4ALevels.getLevels(gameId);

        if (needsToHaveScreens) {
            levels = await this.filterLevelsWithScreens(gameId, levels)
        }

        return levels;
    }

    private static async filterLevelsWithScreens(gameId: number, levels: Level[]) {
        return await Promise.all(levels.filter(async summaryLevel => {
            const level = await H4ALevels.getLevel(gameId, summaryLevel.name);
            return !!level?.mainScreenshotID;
        }))
    }

}