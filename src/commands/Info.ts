import BlessedAPI from "blessed-api";
import { GameWithThumbnail, GlobalGameWithThumbnail } from "blessed-api/dist/eternalfest/v1/types/eternalfest.game.types";
import { Leaderboard } from "blessed-api/dist/eternalfest/v1/types/eternalfest.leaderboard.types";
import { EternalfestGlobalUser } from "blessed-api/dist/eternalfest/v1/users/types/users.options";
import Users from "../cache/Users";
import { eternalfestGameCache, tempEternalfestUsersCache } from "../cache/eternalfest.cache";
import Canvas from "canvas";
import { ApplicationCommandOptionType, AutocompleteInteraction, CommandInteraction, EmbedBuilder } from "discord.js";
import { Discord, Slash, SlashGroup, SlashOption } from "discordx";
import BotError from "../errors/BotError";

interface PlayerRanking {
    "thumbnail": Buffer,
    "rank": number,
    "isTied": boolean
}

@Discord()
@SlashGroup({
    "name": "info",
    "description": "Donne des informations au joueur"
})
@SlashGroup("info")
export class Info {

    private readonly GAMES_BLACKLIST = [
        "b0c6fc75-d001-42fa-8165-e0b5e5c4221e", // Soccerfest Opta,
        "dd9d21ef-2bf5-4d80-a71e-79299f617cdd" // Soccerfest
    ]

    async getUser(userID: string) {
        const users = await tempEternalfestUsersCache.getUsers();
        const user = users.find(user => user.id === userID);
        return user;
    }

    async checkIfUserIsValid(userID: string): Promise<void> | never {
        const users = await tempEternalfestUsersCache.getUsers();
        const user = users.find(user => user.id === userID);
        if (!user) {
            throw new BotError("Le joueur n'existe pas");
        }
    }

    async getGames(globalGames: GlobalGameWithThumbnail[]) {
        let games: Array<GameWithThumbnail> = [];
        for (const globalGame of globalGames) {
            const game = await eternalfestGameCache.getGameByID(globalGame.id);
            if (this.GAMES_BLACKLIST.includes(game.id)) {
                continue;
            }
            games.push(game);
        }
        return games;
    }

    async getLeaderboards(games: GameWithThumbnail[]) {
        let leaderboards: Array<Leaderboard> = [];
        for (const game of games) {
            const leaderboard = await eternalfestGameCache.getLeaderboardByGameMode(game.id);
            leaderboards.push(leaderboard);
        }
        return leaderboards;
    }

    async getAllPlayerRanking(player: EternalfestGlobalUser): Promise<PlayerRanking[]> {
        const globalGames = await eternalfestGameCache.getGames();

        const games = await this.getGames(globalGames);
        const leaderboards = await this.getLeaderboards(games);

        return leaderboards.map((leaderboard, index) => {
            const {rank, isTied} = this.getPlayerRankFromLeaderboard(leaderboard, player.id);
            return {
                "thumbnail": games[index].thumbnail,
                "rank": rank === 0 ? Infinity : rank,
                isTied
            }
        })
        .sort((a, b) => {
            if (a.rank !== b.rank) {
                return a.rank - b.rank;
            }

            if (a.isTied === b.isTied) {
                return 0;
            }

            if (!a.isTied && b.isTied) {
                return -1;
            } else {
                return 1;
            }
        })
    }

    private getPlayerRankFromLeaderboard(leaderboard: Leaderboard, playerID: string) {
        let rank = leaderboard.results.findIndex(playerInLeaderboard => playerInLeaderboard.user.id === playerID) + 1;

        if (rank === 0) {
            return {
                rank: Infinity,
                isTied: false
            }
        }

        let isTied = false
        while(rank > 1 && leaderboard.results[rank - 2]?.score === leaderboard.results[rank - 1]?.score) {
            rank--;
            isTied = true
        }

        return {
            rank,
            isTied
        }
    }

    @Slash({
        "name": "leaderboard",
        "description": "Donne le classement sur toutes les contrées",
        "dmPermission": true
    })
    async leaderboards(
        @SlashOption({
            "name": "player",
            "description": "Joueur sur qui afficher le classement",
            "required": true,
            "type": ApplicationCommandOptionType.String,
            "autocomplete": async (interaction: AutocompleteInteraction) => {
                const input = interaction.options.getFocused() + "";
                interaction.respond(await Users.autocomplete(input, "id"));
            }
        })
        playerID: string,
        interaction: CommandInteraction
    ) {

        await interaction.deferReply();

        await this.checkIfUserIsValid(playerID);
        const user = await this.getUser(playerID);
        if (!user) {
            throw new BotError("Le joueur n'existe pas");
        }

        const ranks = await this.getAllPlayerRanking(user);

        const outputEmbed = this.buildEmbed(user, ranks);
        const recapImage = await this.drawImage(ranks);

        await interaction.editReply({
            "embeds": [outputEmbed],
            "files": [{
                "name": "image.png",
                "attachment": recapImage
            }]
        });
    }

    buildEmbed(user: EternalfestGlobalUser, playerRankings: PlayerRanking[]) {
        const nonRanked = playerRankings.filter(x => x.rank === Infinity);
        const len = playerRankings.length;

        return new EmbedBuilder()
            .setTitle(`Récapitulatif (${user.display_name})`)
            .setAuthor({
                "name": user.display_name,
            })
            .setTimestamp()
            .setImage('attachment://image.png')
            .addFields([
                {
                    "name": "1er",
                    "value": `${playerRankings.filter(x => x.rank === 1).length}/${len}`,
                    "inline": true
                },
                {
                    "name": "Top 2",
                    "value": `${playerRankings.filter(x => x.rank <= 2).length}/${len}`,
                    "inline": true
                },
                {
                    "name": "Top 3",
                    "value": `${playerRankings.filter(x => x.rank <= 3).length}/${len}`,
                    "inline": true
                },
                {
                    "name": "Top 10",
                    "value": `${playerRankings.filter(x => x.rank <= 10).length}/${len}`,
                    "inline": true
                },
                {
                    "name": "Top 100",
                    "value": `${playerRankings.filter(x => x.rank <= 100).length}/${len}`,
                    "inline": true
                },
                {
                    "name": "\u200b",
                    "value": "\u200b",
                },
                {
                    "name": `Supérieur à 100`,
                    "value": `${playerRankings.filter(x => x.rank > 100 && x.rank !== Infinity).length}/${len}`,
                    "inline": true
                },
                {
                    "name": `Non classé`,
                    "value": `${nonRanked.length}/${len}`,
                    "inline": true
                }
            ]);
    }

    async drawImage(playerRankings: PlayerRanking[]) {
        const numberOfRankings = playerRankings.length;

        const miniaturesPerRow = 10;
        const miniaWidth = 100;
        const miniaHeight = 100;
        const miniaturesPerColumn = Math.ceil(numberOfRankings / miniaturesPerRow);

        const imageWidth = miniaWidth * miniaturesPerRow;
        const imageHeight = miniaHeight * miniaturesPerColumn;

        const canvas = Canvas.createCanvas(imageWidth, imageHeight);
        const context = canvas.getContext('2d');

        for (let i = 0; i < numberOfRankings; i++) {

            const x = (i % miniaturesPerRow) * miniaWidth;
            const y = Math.floor(i / miniaturesPerRow) * miniaHeight;
            const {rank, isTied} = playerRankings[i];
            const thumbnail = playerRankings[i].thumbnail;

            const buffer = await BlessedAPI.Utils.Image.resize(thumbnail, {
                "width": miniaWidth,
                "height": miniaHeight
            });

            const thumbnailCanvas = await Canvas.loadImage(buffer);
            context.drawImage(thumbnailCanvas, x, y, miniaWidth, miniaHeight);
            
            context.fillStyle = 'rgba(0, 0, 0, 0.5)';
            context.fillRect(
                x + miniaWidth / 8,
                y + miniaWidth / 4,
                6 * miniaWidth / 8,
                miniaHeight / 2
            )

            let text: string;
            switch(rank) {
                case 1:
                    context.fillStyle = '#C5BC42';
                    text = "1";
                    break;
                case 2:
                    context.fillStyle = '#d7d7d7';
                    text = "2";
                    break;
                case 3:
                    context.fillStyle = '#977547';
                    text = "3";
                    break;
                default:
                    context.fillStyle = 'white';
                    text = rank === Infinity ? "NC" : rank.toString()
            }
            if (isTied && rank !== Infinity) {
                text += "="
            }

            context.font = 'bold 35px Franklin Gothic Medium Condensed';
            context.textAlign = 'center'; 
            context.textBaseline = 'middle';

            context.fillText(text, x + miniaWidth / 2, y + miniaHeight / 2);
        }

        return canvas.toBuffer();
    }

}
