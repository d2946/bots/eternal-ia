export interface StoredError {
    errorMessage: string;
    debugMessage?: string;
    stackTrace?: string;
}