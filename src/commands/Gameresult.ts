import { LangParameters, Languages } from "../modules/Language";
import { Discord, Slash, SlashOption } from "discordx";
import { ApplicationCommandOptionType, CommandInteraction } from "discord.js";
import BotError from "../errors/BotError";

export const GAMERESULT_LINK = "https://gameresult.ef.rainbowda.sh"

@Discord()
export class Gameresult {
    @Slash({
        "name": "gameresult",
        "description": "Affiche le résultat d'une partie",
    })
    public async gameresult(
        @SlashOption({
            "name": "game-id",
            "description": "ID de la partie",
            "required": true,
            "type": ApplicationCommandOptionType.String,
            "minLength": 36, // 36 is the length of a UUID
            "maxLength": 100
        })
        gameId: string,
        @LangParameters("language", false)
        language: Languages | undefined,
        interaction: CommandInteraction
    ) {
        const ID = gameId.split("/").pop();

        interaction.reply({
            "content": getGameresultLink(ID ?? gameId, language)
        });
    }
        
}

export function getGameresultLink(gameID: string, language?: Languages) {
    if (gameID.includes(" ")) {
        throw new BotError("L'ID de la partie ne doit pas contenir d'espace")
    }

    const endID = gameID.split("/").pop() ?? gameID;
    const id = endID.split("?")[0];

    if (! isUUID(id)) {
        throw new BotError("L'ID de la partie n'est pas valide")
    }

    return `${GAMERESULT_LINK}/${id}?lang=${language ?? "fr"}`;
}

function isUUID(uuid: string) {

    const hexChars = "0123456789abcdef";

    if (uuid.length !== 36) {
        return false;
    }

    if (uuid[8] !== "-" || uuid[13] !== "-" || uuid[18] !== "-" || uuid[23] !== "-") {
        return false;
    }

    // Check that all characters are valid hex characters
    for (let i = 0; i < uuid.length; i++) {
        if (i === 8 || i === 13 || i === 18 || i === 23) {
            continue;
        }

        if (! hexChars.includes(uuid[i])) {
            return false;
        }
    }

    return true;
}
