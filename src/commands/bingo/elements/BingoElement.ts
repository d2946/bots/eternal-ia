export abstract class BingoElement {
    abstract display(): Promise<Buffer>;
    abstract equals(other: BingoElement): boolean;
}

export abstract class BingoElementFactory {

    difficulty: number;
    allowDuplicates: boolean;
    max: number;

    constructor(difficulty: number, allowDuplicates: boolean, max: number) {
        if (difficulty <= 0) {
            throw new Error("Difficulty can't be negative")
        }
        this.difficulty = difficulty;
        this.allowDuplicates = allowDuplicates;
        this.max = Math.max(max, 0);
    }

    decrementMax(): number {
        return --this.max
    }

    abstract generate(): BingoElement
}