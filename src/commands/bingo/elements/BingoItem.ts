import {BingoElement, BingoElementFactory} from "./BingoElement";
import ArrayModule from "blessed-api/dist/utils/ArrayUtils"
import BlessedAPI from "blessed-api";
import { Item } from "@hamm4all/shared";
import { getAsset } from "../../../cache/hamm4all.cache";
import BotError from "../../../errors/BotError";

class BingoItem extends BingoElement {

    public readonly item: Item;

    constructor(item: Item) {
        super()
        this.item = item;
    }

    async display(): Promise<Buffer> {
        if (! this.item.picture_id) {
            throw new BotError("Un des items sélectionnés par le Bingo n'a pas d'image associée")
        }
        const image = await getAsset(this.item.picture_id)
        return await BlessedAPI.Utils.Image.resize(image, {"width": 40, "height": 40});
    }

    equals(other: BingoElement): boolean {
        if (other instanceof BingoItem) {
            if (this.item.id === other.item.id) {
                return true;
            }
        }
        return false
    }
}

class BingoItemFactory extends BingoElementFactory {

    public readonly items: Array<Item> = []

    constructor(weight: number, totalWeights: number, items: Array<Item>, max: number) {
        super(totalWeights / weight, false, max);
        this.items = items;
    }

    public override generate(): BingoItem {
        const randomItem = ArrayModule.randomElement(this.items)
        return new BingoItem(randomItem)
    }
}

export {
    BingoItemFactory
}