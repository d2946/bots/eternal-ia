import { CommandInteraction, GuildMember, ButtonInteraction, ApplicationCommandOptionType, EmbedBuilder, ButtonBuilder, ButtonStyle, ActionRowBuilder, Colors, Embed, Attachment, userMention } from "discord.js";
import sharp from "sharp";
import { BINGO_PATH } from "./Bingo";
import fs from "fs"
import { CommunityBingo } from "../../database/models/CommunityBingo";
import { CommunityBingoBox } from "../../database/models/CommunityBingoBox";
import { ButtonComponent, Discord, Guard, Slash, SlashGroup, SlashOption } from "discordx";
import BotError from "../../errors/BotError";
import { getGrade, hasPermission, NeedsPermission, toGrade, UserPermission } from "../../permissions/Permissions";
import { EmbedService } from "../../embeds/embed.service";
import PermissionError from "../../errors/PermissionError";
import { BINGO_BOX_SIZE } from "./BingoGenerate";
import Connection from "../../database/Connection";
import { IsNull, Not } from "typeorm";

const DENY_BINGO_TICK_ID = "deny-bingo-tick"
const ACCEPT_BINGO_TICK_ID = "accept-bingo-tick"

const acceptOrDenyPermission: UserPermission = "MODERATOR"

const HIGHLIGHTED_ATTACHMENT_NAME = "highlighted.png"

@Discord()
@SlashGroup("bingo")
export class BingoTick {
    private readonly communityBingoRepository = Connection.getRepository(CommunityBingo);

    @Slash({
        "name": "tick",
        "description": "Coche une case du bingo communautaire",
    })
    async tick(
        @SlashOption({
            "name": "row",
            "description": "Lettre de la ligne de la case à cocher (commence à A, de haut en bas)",
            "type": ApplicationCommandOptionType.String,
            "required": true
        })
        row: string,
        @SlashOption({
            "name": "column",
            "description": "Numéro de la colonne de la case à cocher (commence à 1, de gauche à droite)",
            "type": ApplicationCommandOptionType.Integer,
            "required": true,
            "minValue": 1
        })
        x: number,
        @SlashOption({
            "name": "screen",
            "description": "Capture d'écran prouvant la case cochée",
            "type": ApplicationCommandOptionType.Attachment,
            "required": true
        })
        proof: Attachment,
        interaction: CommandInteraction
    ) {
        await interaction.deferReply()
        row = row.toUpperCase()

        const globalGrid = await this.getGrid()

        const y = row.charCodeAt(0) - 65 + 1
        if (y < 1) {
            throw new BotError(`La lettre ${row} n'est pas valide. Elle doit être comprise entre A et ${String.fromCharCode(65 + globalGrid.size - 1)}`)
        }

        if (x > globalGrid.size || y > globalGrid.size) {
            throw new BotError(`La case ${row}${x} n'existe pas. Vérifie qu'aucune coordonnée ne dépasse la taille de la grille (${globalGrid.size})`)
        }

        const {grid} = globalGrid

        const box = grid.find(b => b.x === x && b.y === y)!
        if (box.ticked_by) {
            throw new BotError(`La case ${row}${x} est déjà cochée`)
        }

        const embed = new EmbedBuilder()
            .setDescription(`Demande de ${userMention(interaction.user.id)}`)
            .setThumbnail(interaction.user.avatarURL() ?? interaction.user.defaultAvatarURL)
            .setTitle(`Cocher la case **${row}${x}**`)
            .setImage(`attachment://${proof.name}`)
            .setTimestamp()

        const accept = new ButtonBuilder()
            .setCustomId(ACCEPT_BINGO_TICK_ID)
            .setLabel("Accepter")
            .setStyle(ButtonStyle.Success)

        const deny = new ButtonBuilder()
            .setCustomId(DENY_BINGO_TICK_ID)
            .setLabel("Refuser")
            .setStyle(ButtonStyle.Danger)

        const actionRow = new ActionRowBuilder<ButtonBuilder>()
            .addComponents(accept, deny)

        // Get current bingo image
        const currentBingoImage = await getCurrentBingoImage()

        // Highlight the box which is being ticked
        const highlightedImage = await highlightBox(currentBingoImage, x, y)

        // Send message to the bingo channel
        await interaction.editReply({
            "embeds": [embed],
            "components": [actionRow],
            "files": [proof, {
                "name": HIGHLIGHTED_ATTACHMENT_NAME,
                "attachment": highlightedImage
            }],
        })
    }

    async getGrid() {
        const grid = await CommunityBingo.findOne({
            "where": {
                "ended_at": IsNull(),
                "created_at": Not(IsNull())
            },
            "relations": {
                "grid": true
            }
        })

        if ((! grid) || grid.ended_at) {
            throw new BotError("Aucune partie de Bingo communautaire n'est en cours")
        }

        if (grid.grid.length === 0) {
            const boxes = await CommunityBingoBox.find({
                "where": {
                    "id_bingo": grid.id
                }
            });
            grid.grid = boxes;
        }

        return grid;
    }

    @ButtonComponent({ "id": ACCEPT_BINGO_TICK_ID })
    @Guard(NeedsPermission(acceptOrDenyPermission))
    async accept(interaction: ButtonInteraction) {
        if (! await hasPermission(interaction.user.id, acceptOrDenyPermission)) {
            const grade = await getGrade(interaction.user.id)
            throw new PermissionError(grade, toGrade(acceptOrDenyPermission))
        }

        const messageEmbed = interaction.message.embeds[0]
        const {x, y} = this.getCoordsFromEmbed(messageEmbed)
        const grid = await this.getGrid()
        // Get user the message got interacted with
        const memberID = messageEmbed.description?.match(/<@!?(\d+)>/)?.[1]!
        const member = interaction.guild!.members.cache.get(memberID) as GuildMember
        const {updatedGrid, bingo} = await tick(grid, member, x, y)
        
        // Fetch the proof screenshot
        const files = interaction.message.attachments.map(a => {
            return {
                "name": a.name!,
                "attachment": a.url
            }
        })

        const embed = new EmbedBuilder(messageEmbed.toJSON())
            .setColor(Colors.Green)
            .setTimestamp()
            .setFooter({
                "text": `Accepté par ${interaction.member?.user.username}`,
                "iconURL": interaction.user.avatarURL() ?? interaction.user.defaultAvatarURL
            })

        const toUpdate: any = {
            "embeds": [embed],
            components: [],
            files: files.filter(f => f.name !== HIGHLIGHTED_ATTACHMENT_NAME)
        }

        if (updatedGrid) {
            toUpdate.files?.push(updatedGrid)
        }
        if (bingo) {
            const successBingo = EmbedService.buildSuccess({
                "title": "Bingo !",
                "description": `Le bingo vient d'être complété !`
            })
            toUpdate.embeds!.push(successBingo);
        }

        await interaction.update(toUpdate)
    }

    getCoordsFromEmbed(embed: Embed): {x: number, y: number} {
        const title = embed.title as string
        const boxName = (/\*\*([A-Z])(\d+)\*\*/).exec(title);

        if (! boxName) {
            throw new BotError("Impossible de récupérer les coordonnées de la case")
        }

        return {
            "y": parseInt(boxName[1].charCodeAt(0).toString()) - 65 + 1,
            "x": parseInt(boxName[2])
        }
    }

    @ButtonComponent({ "id": DENY_BINGO_TICK_ID })
    @Guard(NeedsPermission(acceptOrDenyPermission))
    async deny(interaction: ButtonInteraction) {

        if (! await hasPermission(interaction.user.id, acceptOrDenyPermission)) {
            const grade = await getGrade(interaction.user.id)
            throw new PermissionError(grade, toGrade(acceptOrDenyPermission))
        }

        const messageEmbed = interaction.message.embeds[0]
        const embed = new EmbedBuilder(messageEmbed.toJSON())
            .setColor(Colors.Red)
            .setTimestamp()
            .setFooter({
                "text": `Refusé par ${interaction.user.username}`,
                "iconURL": interaction.user.avatarURL() ?? interaction.user.defaultAvatarURL
            })

        await interaction.update({
            "embeds": [embed],
            components: [],
            files: []
        })
    }


}

async function highlightBox(image: Buffer, x: number, y: number): Promise<Buffer> {

    const margin = BINGO_BOX_SIZE / 5;

    const highlightBox = await sharp({
        "create": {
            "width": BINGO_BOX_SIZE + margin,
            "height": BINGO_BOX_SIZE + margin,
            "channels": 4,
            "background": { "r": 255, "g": 230, "b": 0, "alpha": 0.7 },
        }
    })
    .png()
    .toBuffer()

    const highlighted = await sharp(image)
        .composite([
            {
                "left": Math.floor(x * BINGO_BOX_SIZE - margin / 2),
                "top": Math.floor(y * BINGO_BOX_SIZE - margin / 2),
                "input": highlightBox,
            }
        ])
        .png()
        .toBuffer()

    return highlighted;
}

async function getCurrentBingoImage() {
    return fs.readFileSync(BINGO_PATH);
}

async function tick(grid: CommunityBingo, member: GuildMember, x: number, y: number): Promise<{updatedGrid: Buffer, bingo: boolean}> {
    const avatar = member.avatarURL() ?? member.user.avatarURL() ?? member.user.defaultAvatarURL
    const avatarBuffer = await fetch(avatar).then(r => r.blob()).then(b => b.arrayBuffer()).then(b => Buffer.from(b))

    const resizedAvatar = await sharp(avatarBuffer)
        .resize(BINGO_BOX_SIZE, BINGO_BOX_SIZE)
        .png()
        .toBuffer()

    const currentBingoGrid = fs.readFileSync(BINGO_PATH);
    const newGrid = await sharp(currentBingoGrid)
        .composite([
            {
                "left": x * BINGO_BOX_SIZE,
                "top": y * BINGO_BOX_SIZE,
                "input": resizedAvatar,
            }
        ])
        .png()
        .toBuffer()

    fs.writeFileSync(BINGO_PATH, newGrid)

    const box = grid.grid.find(b => b.x === x && b.y == y)!
    box.ticked_by = member.id;
    box.ticked_at = new Date();
    await box.save();

    const bingoTriggers = [
        grid.grid.filter(b => b.y === y), // Row
        grid.grid.filter(b => b.x === x), // Column
        grid.grid.filter(b => b.x === b.y), // Diagonal 1
        grid.grid.filter(b => b.x === grid.size - b.y + 1) // Diagonal 2
    ]

    for (const trigger of bingoTriggers) {
        if (checkBingo(trigger)) {
            finishBingo(grid);
            return {
                "updatedGrid": newGrid,
                bingo: true
            }
        }
    }

    return {
        "updatedGrid": newGrid,
        "bingo": false
    }
}

function checkBingo(ticked: Array<CommunityBingoBox>) {
    return ticked.every(t => t.ticked_by !== null && t.ticked_by !== undefined);
}

async function finishBingo(grid: CommunityBingo) {
    grid.ended_at = new Date();
    await grid.save();
}
