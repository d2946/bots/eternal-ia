import { ApplicationCommandOptionChoiceData, ApplicationCommandOptionType, AutocompleteInteraction, CommandInteraction, EmbedBuilder } from "discord.js";
import { Discord, Slash, SlashOption } from "discordx";
import sharp, { OverlayOptions } from "sharp";
import Users from "../../cache/Users";
import BotError from "../../errors/BotError";
import { eternaltwinUsersCache } from "../../cache/eternaltwin.cache";
import H4AItems from "../../core/api/hamm4all/items-api";
import H4AFiles from "../../core/api/hamm4all/files-api";
import EFFridge, { EternalfestFridge } from "../../core/api/eternalfest/fridge-api";
import Games from "../../cache/Games";
import { eternalfestGameCache } from "../../cache/eternalfest.cache";
import { ETERNALFEST_GAMES_URL } from "../../Const";

async function autocompleteFunction(
    autocompleteInteraction: AutocompleteInteraction,
    callback: (autocompleteInteraction: AutocompleteInteraction) => Promise<ApplicationCommandOptionChoiceData[]>
) {
    const time = Date.now()
    const result = await callback(autocompleteInteraction)
    const endTime = Date.now();
    const timeEllapsedInMilliseconds = endTime - time;
    if (timeEllapsedInMilliseconds >= 2800) {
        return;
    }
    await autocompleteInteraction.respond(result)
}

@Discord()
export class TowerOfGreed {

    static readonly MAX_ITEMS_PER_LINE = 100

    async getPlayerAndCheckIfPlayerExists(playerID: string) {
        const player = await eternaltwinUsersCache.getUser(playerID)
            .catch(() => null);
        if (! player) {
            throw new BotError(`Je n'ai pas trouvé de joueur avec l'ID \`${playerID}\``)
        }

        return player
    }

    getCoefficientsToKeep(keepCoeff7: boolean) {
        const coefficientsToKeep = [5, 6];
        if (keepCoeff7) {
            coefficientsToKeep.push(7)
        }

        return coefficientsToKeep
    }

    @Slash({
        "name": "greed",
        "description": "Renvoie la Tower of Greed du frigo demandé"
    })
    async tog(
        @SlashOption({
            "name": "player-name-or-id",
            "description": "Nom du joueur",
            "required": true,
            "type": ApplicationCommandOptionType.String,
            "autocomplete": (interaction) => autocompleteFunction(
                interaction,
                async (interaction) => {
                    const input = interaction.options.getFocused().toString()
                    const users = await Users.autocompleteNameAndID(input);
                    return users
                }
            ) 
        })
        playerID: string,
        @SlashOption({
            "name": "game-name",
            "description": "Nom du jeu",
            "required": true,
            "type": ApplicationCommandOptionType.String,
            "autocomplete": (interaction) => autocompleteFunction(
                interaction,
                async interaction => {
                    const input = interaction.options.getFocused().toString()
                    const games = await Games.autocomplete(input);
                    return games;
                }
            )
        })
        gameID: string,
        @SlashOption({
            "name": "no-flocon-enorme",
            "description": "N'affiche pas le flocon énorme",
            "required": false,
            "type": ApplicationCommandOptionType.Boolean
        })
        noFloconEnorme: boolean,
        @SlashOption({
            "name": "include-coeff-7",
            "description": "Affiche également les coefficients 7",
            "required": false,
            "type": ApplicationCommandOptionType.Boolean
        })
        includeCoeff7: boolean,
        @SlashOption({
            "name": "display-amount",
            "description": "Affiche la quantité d'objets sur la ligne",
            "required": false,
            "type": ApplicationCommandOptionType.Boolean
        })
        displayAmount: boolean,
        interaction: CommandInteraction
    ) {
        await interaction.deferReply();

        const player = await this.getPlayerAndCheckIfPlayerExists(playerID)

        let fridge: EternalfestFridge
        try {
            fridge = await EFFridge.getFridge(gameID ?? "0dc0d559-de83-4e0c-982d-fc56100dfdd5", playerID)
        }
        catch (e) {
            console.error(e)
            throw new BotError(`Je n'ai pas réussi à récupérer le frigo de \`${player.display_name.current.value}\``)
        }

        const coefficientsToKeep = this.getCoefficientsToKeep(includeCoeff7)

        const image = await this.getImage(fridge, coefficientsToKeep, noFloconEnorme, displayAmount)
        const game = await eternalfestGameCache.getGameByID(gameID)

        const embed = new EmbedBuilder()
            .setTitle("Tower of Greed")
            .setAuthor({
                "name": player.display_name.current.value,
            })
            .addFields([
                {
                    "name": "Jeu",
                    "value": `[${game.channels.active.build.display_name}](${ETERNALFEST_GAMES_URL}/${gameID})`
                }
            ])
            .setColor([100, 200, 100])
            .setImage('attachment://image.png')
        
        await interaction.editReply({
            "embeds": [embed],
            "files": [{name: "image.png", attachment: image}]
        })
    }

    async getImage(fridge: EternalfestFridge, coefficientsToKeep: Array<number>, noFloconEnorme: boolean, displayAmount: boolean) {
        let composites: Array<OverlayOptions> = [];
        const ITEM_SIZE = 30
        const rawImages = await H4AItems.getItems();
        const userImages = await Promise.all(rawImages
            .filter(item => {
                if (noFloconEnorme) {
                    return item.name.fr !== "Flocon ENORME !"
                }
                return true
            }) // Remove flocon énorme if the option is set
            .filter(item => coefficientsToKeep.includes(item.coefficient))
            .filter(item => fridge[item.in_game_id] !== undefined) // Only images in the fridge
            .filter(item => !!item.picture_id) // Only items that have an image
            .map(async item => {
                return {
                    "id": item.in_game_id,
                    "image": await H4AFiles.getResizedFile(item.picture_id!, ITEM_SIZE, ITEM_SIZE)
                }
            })
        )
        .then((images) => images.toSorted((a, b) => fridge[a.id] - fridge[b.id]))

        if (userImages.length === 0) {
            throw new BotError(`Aucun objet à afficher n'a été trouvé dans le frigo du joueur concerné`)
        }

        const maxWidth = Math.min(fridge[userImages[userImages.length - 1].id] * ITEM_SIZE, TowerOfGreed.MAX_ITEMS_PER_LINE * ITEM_SIZE) + (displayAmount ? 2 * ITEM_SIZE : 0);

        const width = maxWidth;
        const height = (userImages.length + 1) * ITEM_SIZE;
        const image = await sharp({
            "create": {
                width,
                height,
                "background": {
                    "r": 0,
                    "g": 0,
                    "b": 0,
                    "alpha": 0
                },
                "channels": 4
            }
        })

        for(let y = 0 ; y < userImages.length ; y++) {
            const im = userImages[y]
            const amount = Math.min(fridge[im.id], TowerOfGreed.MAX_ITEMS_PER_LINE)
            for(let x = 0 ; x < amount ; x++) {
                const width = 30 * amount;
                composites.push({
                    "top": 30 * y,
                    "left": (maxWidth / 2) - width / 2 + ITEM_SIZE * x,
                    "input": im.image
                })
            }

            if (displayAmount) {
                composites.push({
                    "input": Buffer.from(this.getTextAmount(amount, ITEM_SIZE)),
                    "top": ITEM_SIZE * y,
                    "left": maxWidth - ITEM_SIZE
                })
                composites.push({
                    "input": Buffer.from(this.getTextAmount(amount, ITEM_SIZE)),
                    "top": ITEM_SIZE * y,
                    "left": 0
                })
            }
        }
        
        return image
            .composite(composites)
            .png()
            .toBuffer()
    }

    getTextAmount(amount: number, size: number) {
        return `<svg width="${size}" height="${size}" xmlns="http://www.w3.org/2000/svg">
            <rect width="100%" height="100%" fill="transparent"/>
            <text x="50%" y="85%" font-size="24" text-anchor="middle" fill="white" stroke="white" stroke-width="2">${amount}
            </text>
        </svg>`
    }

    @Slash({
        "name": "full-greed",
        "description": "Renvoie la Tower of Greed du frigo demandé"
    })
    async fullGreed(
        @SlashOption({
            "name": "player-name-or-id",
            "description": "Nom du joueur",
            "required": true,
            "type": ApplicationCommandOptionType.String,
            "autocomplete": (interaction) => autocompleteFunction(
                interaction,
                async (interaction) => {
                    const input = interaction.options.getFocused().toString()
                    const users = await Users.autocompleteNameAndID(input);
                    return users
                }
            ) 
        })
        playerID: string,
        @SlashOption({
            "name": "no-flocon-enorme",
            "description": "N'affiche pas le flocon énorme",
            "required": false,
            "type": ApplicationCommandOptionType.Boolean
        })
        noFloconEnorme: boolean,
        @SlashOption({
            "name": "include-coeff-7",
            "description": "Affiche également les coefficients 7",
            "required": false,
            "type": ApplicationCommandOptionType.Boolean
        })
        includeCoeff7: boolean,
        @SlashOption({
            "name": "display-amount",
            "description": "Affiche la quantité d'objets sur la ligne",
            "required": false,
            "type": ApplicationCommandOptionType.Boolean
        })
        displayAmount: boolean,
        interaction: CommandInteraction
    ) {
        await interaction.deferReply();

        const FULL_GREED_BLACKLIST = [
            "e1dfc653-2ee5-434a-850f-007359acf738", // NDJ
            "2e63b3bf-769d-4f17-bb0c-68542ecd7260", // Supplice
            "474fa18e-8d92-4dba-bb78-1ba277fbedd5", // Ololzd2
        ]

        const player = await this.getPlayerAndCheckIfPlayerExists(playerID)
        const games = await eternalfestGameCache.getGames()
        let fullFridge: EternalfestFridge = []

        for(const game of games) {
            if (FULL_GREED_BLACKLIST.includes(game.id)) {
                continue;
            }

            const fridge = await EFFridge.getFridge(game.id, playerID)
            for (const [key, value] of Object.entries(fridge)) {
                const integerKey = parseInt(key)
                if (fullFridge[integerKey]) {
                    fullFridge[integerKey] += value
                } else {
                    fullFridge[integerKey] = value
                }
                
            }
        }

        const coefficientsToKeep = this.getCoefficientsToKeep(includeCoeff7)

        const image = await this.getImage(fullFridge, coefficientsToKeep, noFloconEnorme, displayAmount)

        const embed = new EmbedBuilder()
            .setTitle("Tower of Greed")
            .setAuthor({
                "name": player.display_name.current.value,
            })
            .setColor([100, 200, 100])
            .setImage('attachment://image.png')
        
        await interaction.editReply({
            "embeds": [embed],
            "files": [{name: "image.png", attachment: image}]
        })
    }
}
