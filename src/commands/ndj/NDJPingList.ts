/**
 * Returns a string representing the time in the format HH:MM
 * @param time The time in minutes
 * @returns 
 */
export function toHHhMM(time: number): string {
    const hours: number = Math.floor(time / 60);
    const minutes: number = time % 60;
    const paddedMinutes = ("" + minutes).padStart(2, "0");
    return `${hours}h${paddedMinutes}`;
}