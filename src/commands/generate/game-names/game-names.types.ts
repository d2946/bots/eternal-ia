export interface Noun {
    "singular": string,
    "plural": string,
    "genre": "m" | "f"
}

export interface Adjective {
    "singular": {
        "m": string,
        "f": string
    }
    "plural": {
        "m": string,
        "f": string
    }
}

export const articles = {
    "m": "le",
    "f": "la",
    "plural": "les"
}

export const prepositions = {
    "m": "du",
    "f": "de la",
    "plural": "des"
}