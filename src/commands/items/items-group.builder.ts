import { ItemGroupColor, ItemInGroup } from "./types/items-group.types";
import Canvas from "canvas";

export class ItemGroupBuilder {

    private sizeOfItem: number;
    private sizeOfText: number;
    private font: string;
    private padding: number;
    private fontColor: ItemGroupColor;
    private missingItemColor: ItemGroupColor;
    private maxItemsPerRow: number;
    private displayAmount: boolean;

    constructor() {
        this.sizeOfItem = 50;
        this.sizeOfText = 22;
        this.font = "Arial";
        this.padding = 5;
        this.fontColor = "white";
        this.missingItemColor = "yellow";
        this.displayAmount = true;
        this.maxItemsPerRow = 10;
    }

    public setSizeOfItem(size: number): this {
        this.sizeOfItem = size;
        return this;
    }

    public setSizeOfText(size: number): this {
        this.sizeOfText = size;
        return this;
    }

    public setFont(font: string): this {
        this.font = font;
        return this;
    }

    public setMaxItemsPerRow(max: number): this {
        this.maxItemsPerRow = max;
        return this;
    }

    public setPadding(padding: number): this {
        this.padding = padding;
        return this;
    }

    public setFontColor(color: ItemGroupColor): this {
        this.fontColor = color;
        return this;
    }

    public setMissingItemColor(color: ItemGroupColor): this {
        this.missingItemColor = color;
        return this;
    }

    public setDisplayAmount(display: boolean): this {
        this.displayAmount = display;
        return this;
    }

    public async build(items: Array<ItemInGroup>): Promise<Buffer> {
        const paddingBottom = this.padding * 2;

        const rowAmount = Math.ceil(items.length / this.maxItemsPerRow);

        const canvasWidth = Math.min(items.length, this.maxItemsPerRow) * (this.sizeOfItem + this.padding);
        const canvasHeight = rowAmount * (this.sizeOfItem + this.padding);
        const canvas = Canvas.createCanvas(
            canvasWidth,
            canvasHeight + (this.displayAmount ? (this.sizeOfText + this.padding) * rowAmount : 0)
        );

        const ctx = canvas.getContext("2d");

        ctx.font = `${this.sizeOfText}px ${this.font}`;
        ctx.textAlign = "center";
        ctx.textBaseline = "middle";

        for(let i = 0 ; i < items.length ; i++) {
            const rowIndex = Math.floor(i / this.maxItemsPerRow);
            const columnIndex = i % this.maxItemsPerRow;

            const item = items[i];
            const x = columnIndex * (this.sizeOfItem + this.padding);
            const y = rowIndex * (this.sizeOfItem + this.padding + (this.displayAmount ? this.sizeOfText + this.padding : 0));

            const xText = x + this.sizeOfItem / 2;
            const yText = y + this.sizeOfItem / 2;

            const xAmountText = xText;
            const yAmountText = yText + this.sizeOfItem / 2 + paddingBottom + this.sizeOfText / 2;

            ctx.fillStyle = this.fontColor;

            if (! item.img) {
                ctx.fillStyle = this.missingItemColor;
                ctx.fillText("?", xText, yText);
                ctx.fillStyle = this.fontColor;
            } else {
                const image = await Canvas.loadImage(item.img);
                ctx.drawImage(
                    image,
                    x,
                    y,
                    this.sizeOfItem,
                    this.sizeOfItem
                );
            }

            if (this.displayAmount) {
                if (! item.amount) {
                    ctx.fillStyle = this.missingItemColor;
                }
                const amount = (item.amount ? item.amount : "?").toLocaleString("fr-FR"); // The ternary operator is here to deal with undefined and NaN

                ctx.fillText(amount, xAmountText, yAmountText)
            }
        }

        return canvas.toBuffer();
    }

}