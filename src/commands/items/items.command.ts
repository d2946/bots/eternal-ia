import { Discord, Slash, SlashOption } from "discordx";
import { ApplicationCommandOptionType, AutocompleteInteraction, CommandInteraction } from "discord.js";
import BotError from "../../errors/BotError";
import { ItemsService } from "./items.service";
import H4AItems from "../../core/api/hamm4all/items-api";

export const IMAGE_SIZE = 150;

@Discord()
export class ItemCommand {

    @Slash({
        "name": "item",
        "description": "Donne des informations sur l'objet recherché"
    })
    async itemCommand(
        @SlashOption({
            "name": "name",
            "description": "Objet recherché",
            "required": true,
            "type": ApplicationCommandOptionType.String,
            "autocomplete": async (inter: AutocompleteInteraction) => {
                const input = inter.options.getFocused();
                await inter.respond(await H4AItems.autoCompleteItems(input));  
            }
        })
        itemName: string,
        interaction: CommandInteraction
    ) {
        await interaction.deferReply()

        // Remove the language prefix
        itemName = itemName.slice(5);

        const item = await H4AItems.getItemByCondition((item) => {
            return Object.values(item.name).includes(itemName)
        })

        if (! item) {
            throw new BotError("Je n'ai pas trouvé l'objet que tu recherches.");
        }

        const image = await ItemsService.getImage(item, IMAGE_SIZE)
        const embed = await ItemsService.buildDisplay(item, itemName);

        interaction.editReply({
            embeds: [embed],
            files: [{name: "image.png", attachment: image || ""}]
        })
    }
}