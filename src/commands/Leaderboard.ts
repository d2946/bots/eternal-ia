import { ActionRowBuilder, ApplicationCommandOptionType, AutocompleteInteraction, CommandInteraction, SelectMenuInteraction, StringSelectMenuBuilder } from 'discord.js';
import { buildTable } from '../modules/Display';

import Games from '../cache/Games';
import { Discord, SelectMenuComponent, Slash, SlashOption } from 'discordx';
import BotError from '../errors/BotError';
import { GameWithThumbnail } from 'blessed-api/dist/eternalfest/v1/types/eternalfest.game.types';
import { eternalfestGameCache } from '../cache/eternalfest.cache';
import { BotException } from '../errors/bot.exception';
import { PaginationService } from '../modules/pagination/pagination.service';

interface LeaderboardOptions {
    game: GameWithThumbnail;
    showLevel: boolean;
    showID: boolean;
    showGameOptions: boolean;
    gameMode: string;
    interaction: CommandInteraction | SelectMenuInteraction;
}

@Discord()
export class LeaderboardCommand {

    @Slash({
        "name": "rank",
        "description": "Renvoie le classement de tous les joueurs sur la contrée donnée",
    })
    async leaderboard(
        @SlashOption({
            "name": "game-name",
            "description": "Le nom de la contrée",
            "type": ApplicationCommandOptionType.String,
            "required": true,
            "autocomplete": async (inter: AutocompleteInteraction) => {
                const input = inter.options.getFocused() + "";
                await inter.respond(await Games.autocomplete(input));
            }
        })
        gameName: string,
        @SlashOption({
            "name": "show-level",
            "description": "Affiche le niveau de fin de la partie",
            "type": ApplicationCommandOptionType.Boolean,
            "required": false
        })
        showLevel: boolean | undefined,
        @SlashOption({
            "name": "show-id",
            "description": "Affiche l'ID de la partie",
            "type": ApplicationCommandOptionType.Boolean,
            "required": false
        })
        showID: boolean | undefined,
        @SlashOption({
            "name": "show-game-options",
            "description": "Affiche les options utilisées pour la partie",
            "type": ApplicationCommandOptionType.Boolean,
            "required": false
        })
        showGameOptions: boolean | undefined,
        interaction: CommandInteraction,
    ) {
        await interaction.deferReply()

        const game = await eternalfestGameCache.getGameByID(gameName)
            .catch(() => {
                throw new BotException(
                    `La contrée \`${gameName}\` n'existe pas.`,
                    `Il est nécessaire de donner le **nom exact** de la contrée pour qu'elle puisse être trouvée.\nC'est généralement judicieux d'attendre que la contrée apparaisse dans le menu d'autocomplétion, puis cliquer dessus, pour être sûr qu'elle soit bien sélectionnée.`
                )
            })

        const firstPage = await this.getPagination({
            game,
            showLevel: showLevel ?? false,
            showID: showID ?? false,
            showGameOptions: showGameOptions ?? false,
            gameMode: "solo",
            interaction
        })

        await interaction.editReply(firstPage);
        
    }

    async getPagination(options: LeaderboardOptions) {

        const { game, showID, showLevel, showGameOptions, interaction } = options;
        const leaderboards = await eternalfestGameCache.getLeaderboards(game);

        const leaderboard = leaderboards.find(l => l.id === options.gameMode);
        if (!leaderboard) {
            throw new BotError("Ce mode de jeu n'existe pas");
        }

        const title = `Classement de "${game.channels.active.build.display_name}" (mode ${leaderboard.name})`;
        const headers = ["Position", "Joueur", "Score"];
        if (showLevel) {
            headers.push("Niveau");
        }
        if (showID) {
            headers.push("ID de partie");
        }
        if (showGameOptions) {
            headers.push("Options");
        }

        const table = buildTable({
            title,
            headers,
            data: leaderboard.leaderboard.map((rank, index) => {
                const result: Array<string> = [
                    (index + 1).toString(),
                    rank.name,
                    rank.score.toLocaleString("fr-FR")
                ]
                if (showLevel) {
                    result.push(rank.run.max_level.toString());
                }
                if (showID) {
                    result.push(rank.run.id);
                }
                if (showGameOptions) {
                    result.push(rank.run.game_options.join(", "));
                }
                return result;
            })
        });

        const menu = new ActionRowBuilder<StringSelectMenuBuilder>()
            .addComponents(
                new StringSelectMenuBuilder()
                    .setCustomId("leaderboard-menu")
                    .addOptions(leaderboards.map(l => ({
                        "label": l.name,
                        "value": l.id,
                        "type": ApplicationCommandOptionType.String,
                        "default": l.id === options.gameMode
                    })))
            )
        
        const pagination = PaginationService.addPagination({
            "messageId": interaction.id,
            "pagination": {
                "currentPageIndex": 0,
                "pageResolver": (index) => {
                    return {
                        "content": table[index],
                        "components": [menu]
                    }
                },
                "maxPage": table.length
            }
        })

        return pagination;
    }

    @SelectMenuComponent({
        "id": "leaderboard-menu"
    })
    async menu(interaction: SelectMenuInteraction) {
        await interaction.deferReply()

        const [selected] = interaction.values;

        if (! selected) {
            interaction.editReply("Aucune contrée n'a été sélectionnée. Réessaye");
            return;
        }

        const gameName = interaction.message.content.split("\n")[1].match(/"(.*)"/)?.[1];
        if (! gameName) {
            interaction.editReply("Il y a eu un problème dans la récupération de la contrée. Réessaye");
            return
        }

        const games = await eternalfestGameCache.getGames();
        const globalGame = games.find(g => g.channels.items[0].build.display_name === gameName);

        if (! globalGame) {
            interaction.editReply("Il y a eu un problème dans la récupération de la contrée. Réessaye");
            return;
        }

        const game = await eternalfestGameCache.getGameByID(globalGame.id);

        const options: LeaderboardOptions = {
            game,
            showLevel: interaction.message.content.includes("| Niveau"),
            showID: interaction.message.content.includes("| ID de partie"),
            showGameOptions: interaction.message.content.includes("| Options"),
            gameMode: selected,
            interaction
        }

        const pagination = await this.getPagination(options);

        await interaction.editReply(pagination);

        interaction.message.delete();
    }
}
