import { Discord, Slash, SlashOption } from 'discordx';
import { ApplicationCommandOptionType, AutocompleteInteraction, CommandInteraction } from 'discord.js';
import H4AGames from '../../core/api/hamm4all/games-api';
import H4ALevels from '../../core/api/hamm4all/levels-api';
import { ScreenService } from './screen.service';

@Discord()
export class ScreenCommand {
    @Slash({
        "name": "screen",
        "description": "Renvoie le screen du niveau demandé"
    })
    async screen(
        @SlashOption({
            "name": "game-name",
            "description": "Contrée où le niveau se trouve",
            "required": true,
            "type": ApplicationCommandOptionType.String,
            "autocomplete": async (inter: AutocompleteInteraction) => {
                const input = inter.options.getFocused().toString();
                const games = await H4AGames.autoCompleteGames(input)
                await inter.respond(games);
            },
        })
        gameID: string,
        @SlashOption({
            "name": "level-name",
            "description": "Niveau recherché",
            "required": true,
            "type": ApplicationCommandOptionType.String,
            "autocomplete": async (inter: AutocompleteInteraction) => {
                const input = inter.options.getFocused() + "";
                const gameIDAsString = inter.options.getString("game-name") ?? "";
                const gameID = parseInt(gameIDAsString);
                if ((!gameID) || isNaN(gameID)) inter.respond([])

                const levels = await H4ALevels.autoCompleteLevels(input, gameID)
                await inter.respond(levels);
            },
        })
        levelName: string,
        interaction: CommandInteraction
    ) {
        const screenService = new ScreenService(parseInt(gameID), levelName);
        const result = await screenService.perform();

        await interaction.reply(result);
    }
}
