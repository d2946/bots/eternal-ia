import accents from "remove-accents";

type MatchFunction = (name1: string, name2: string) => boolean;

function normalMatch(name1: string, name2: string): boolean {
    return (accents.remove(name1.toLowerCase())).includes(`${accents.remove(name2.toLowerCase())}`);
}

function exactMatch(name1: string, name2: string): boolean {
    return accents.remove(name1.toLowerCase()) === accents.remove(name2.toLowerCase());
}

export {
    normalMatch,
    exactMatch
}
export type {
    MatchFunction
}