import { HammerfactCommand } from "./commands/Hammerfact";
import { LeaderboardCommand } from "./commands/Leaderboard";
import { PromoteCommand } from "./commands/Promote";
import { Award } from "./commands/Award";
import { Claim } from "./commands/Claim";
import { Title } from "./commands/title/Title";
import { TitleGroup } from "./commands/title/TitleGroup";
import { RandomCommand } from "./commands/Random";
import { NDJCommand } from "./commands/NDJ";
import { QuizRankCommand } from "./commands/quiz/QuizRank";
import { BingoGenerateCommand } from "./commands/bingo/BingoGenerate";
import { BingoTick } from "./commands/bingo/BingoTick";
import { QuizStats } from "./commands/quiz/QuizStats";
import { QuizRecapCommand } from "./commands/quiz/QuizRecap";
import { TowerOfGreed } from "./commands/tower-of-greed/tower-of-greed.command";
import { Fridge } from "./commands/Fridge";
import { EternalfestCommand } from "./commands/games/eternalfest.command";
import { ItemCommand } from "./commands/items/items.command";
import { ChangelogCommand } from "./commands/changelog/changelog.command";
import { AdminCommand } from "./commands/admin/admin.command";
import { QuestCommand } from "./commands/quests/quests.command";
import { FamiliesCommand } from "./commands/families/families.command";
import { Info } from "./commands/Info";
import { Generate } from "./commands/generate/Generate";
import { ScreenCommand } from "./commands/screen/screen.command";

interface ManagingTreeType {
    [key: string]: ManagingTreeSubType
}

type ManagingTreeSubType = ManagingTreeType | object

const REGISTERED_COMMANDS: Set<object> = new Set([
    AdminCommand,
    Award,
    BingoGenerateCommand,
    BingoTick,
    ChangelogCommand,
    Claim,
    EternalfestCommand,
    FamiliesCommand,
    Fridge,
    Generate,
    HammerfactCommand,
    NDJCommand,
    Info,
    ItemCommand,
    PromoteCommand,
    QuizRankCommand,
    QuizStats,
    QuizRecapCommand,
    QuestCommand,
    RandomCommand,
    ScreenCommand,
    LeaderboardCommand,
    Title,
    TitleGroup,
    TowerOfGreed
])

export {
    REGISTERED_COMMANDS
}