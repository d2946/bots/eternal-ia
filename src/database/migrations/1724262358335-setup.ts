import { MigrationInterface, QueryRunner } from "typeorm";

export class Setup1724262358335 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`
            CREATE TABLE IF NOT EXISTS NDJ_Pings (
                id_user varchar NOT NULL,
                ping_time integer NOT NULL,
                done boolean NOT NULL DEFAULT false,
                CHECK(ping_time >= 0 AND ping_time < 1440),
                primary key (id_user, ping_time)
            );

            CREATE TABLE IF NOT EXISTS Roles (
                id TEXT NOT NULL,
                coeff REAL NOT NULL DEFAULT 1,
                primary key(id)
            );

            CREATE TABLE IF NOT EXISTS Users (
                id TEXT NOT NULL,
                role REAL NOT NULL DEFAULT 1,
                primary key(id)
            );

            CREATE TABLE IF NOT EXISTS RoleGroups (
                id SERIAL,
                name TEXT UNIQUE,
                primary key(id)
            );

            CREATE TABLE IF NOT EXISTS RoleGroupItems (
                idGroup INTEGER,
                idRole TEXT UNIQUE,
                rank INTEGER,
                primary key(idGroup, idRole),
                foreign key(idGroup) REFERENCES RoleGroups(id) ON DELETE CASCADE,
                foreign key(idRole) REFERENCES Roles(id) ON DELETE CASCADE,
                UNIQUE (idGroup, rank)
            );

            CREATE TABLE IF NOT EXISTS Language (
                id_user TEXT,
                lang TEXT NOT NULL DEFAULT 'fr',
                primary key (id_user)
            );

            CREATE TABLE IF NOT EXISTS Quiz (
                id BIGSERIAL,
                winner TEXT,
                rarity TEXT NOT NULL,
                reward REAL NOT NULL DEFAULT 0,
                created_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
                PRIMARY KEY(id)
            );

            CREATE TABLE IF NOT EXISTS CommunityBingos (
                id SERIAL PRIMARY KEY,
                difficulty REAL NOT NULL,
                size INTEGER NOT NULL,
                created_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP,
                ended_at TIMESTAMP WITH TIME ZONE
            );

            CREATE TABLE IF NOT EXISTS CommunityBingosBoxes (
                id_bingo INTEGER REFERENCES CommunityBingos(id),
                x INTEGER NOT NULL,
                y INTEGER NOT NULL,
                ticked_by TEXT,
                ticked_at TIMESTAMP WITH TIME ZONE,
                PRIMARY KEY(id_bingo, x, y)
            );
        `)
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        queryRunner.query(`
            DROP TABLE NDJ_Pings, Roles, Users, RoleGroups, RoleGroupItems, Language, Quiz, CommunityBingos, CommunityBingosBoxes
        `)
    }

}
