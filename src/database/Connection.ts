import "reflect-metadata"

import { ENV } from "../Environment";
import { logger } from "../Log";
import { DataSource } from "typeorm";

const datasource = new DataSource({
    "type": "postgres",
    "username": ENV.DB_USER,
    "password": ENV.DB_PASSWORD,
    "database": ENV.DB_NAME,
    "host": ENV.DB_HOST,
    "port": ENV.DB_PORT,
    "entities": [__dirname + "/models/**/*.{js,ts}"],
    "migrations": [__dirname + "/migrations/**/*.{js,ts}"],
    "logging": false
});

datasource.initialize()
    .then(() => logger.info("Database synchronized"))

export default datasource;