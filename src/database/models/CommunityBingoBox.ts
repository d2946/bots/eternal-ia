import { BaseEntity, Column, Entity, JoinColumn, ManyToOne, PrimaryColumn, PrimaryGeneratedColumn } from "typeorm";
import { Min } from "class-validator";
import { CommunityBingo } from "./CommunityBingo";

@Entity({
    "name": "communitybingosboxes"
})
export class CommunityBingoBox extends BaseEntity {

    @PrimaryGeneratedColumn("increment")
    declare id_bingo: number;

    @PrimaryColumn({ "type": "integer", "nullable": false })
    @Min(0)
    declare x: number;

    @PrimaryColumn({ "type": "integer", "nullable": false })
    @Min(0)
    declare y: number;

    @Column({ "nullable": true })
    declare ticked_by?: string

    @Column({ "nullable": true })
    declare ticked_at?: Date

    @ManyToOne(() => CommunityBingo)
    @JoinColumn({ "name": "id_bingo" })
    declare bingo: CommunityBingo
}