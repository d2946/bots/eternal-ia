import { Max, Min } from "class-validator";
import datasource from "../Connection";
import { BaseEntity, Column, Entity, PrimaryColumn } from "typeorm";

@Entity({
    "name": "ndj_pings"
})
export class NDJPing extends BaseEntity {

    @PrimaryColumn()
    declare id_user: string

    @PrimaryColumn()
    @Min(0)
    @Max(1440)
    declare ping_time: number;

    @Column({ "type": "boolean", "nullable": false, "default": false })
    declare done: boolean;

    getHour() {
        return Math.floor(this.ping_time / 60);
    }

    getMinute() {
        return this.ping_time % 60;
    }

    getTime() {
        return `${this.getHour().toString().padStart(2, "0")}h${this.getMinute().toString().padStart(2, "0")}`;
    }

    static async getAllPingsAtTime(time: number) {
        return await NDJPing.find({
            "where": {
                "ping_time": time,
                "done": false
            }
        })
    }

    static async resetNDJ(){
        datasource.createQueryBuilder()
            .update(NDJPing)
            .set({ "done": false })
            .where({ "done": true })
            .execute();
    }
}