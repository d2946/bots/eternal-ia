import { BaseEntity, Column, CreateDateColumn, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity({
    "name": "quiz"
})
export class Quiz extends BaseEntity {

    @PrimaryGeneratedColumn("increment")
    declare id: bigint

    @Column({ "nullable": true })
    declare winner?: string;

    @Column({ "nullable": false })
    declare rarity: string

    @Column({ "type": "numeric", "nullable": false, "default": 0})
    declare reward: number;

    @CreateDateColumn()
    declare created_at: Date
}