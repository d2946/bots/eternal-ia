import { RoleGroupItem } from "./RoleGroupItems";
import { GuildMember, Role } from "discord.js";
import { BaseEntity, Column, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";

interface UpgradableRolesType {
    originalRole: Role,
    newRole: Role
}

@Entity({
    "name": "rolegroups"
})
export class RoleGroup extends BaseEntity {
    @PrimaryGeneratedColumn()
    declare id: number;

    @Column({ "nullable": false, "type": "varchar", "length": 255, "unique": true})
    declare name: string;

    @OneToMany(() => RoleGroupItem, (roleGroupItem) => roleGroupItem.group)
    declare roles?: RoleGroupItem[]

    /**
     * Returns all the roles that are in the same rolegroup as the given role
     * @param roleID 
     */
    static async getAllRolesInSameGroup(roleID: string) {
        const roleGroupItem = await RoleGroupItem.findOne({
            "where": {
                "idrole": roleID,
            },
            "relations": {
                "group": {
                    "roles": true
                },
            }
        });

        if (! roleGroupItem) {
            return [];
        }

        return roleGroupItem.group?.roles ?? [];
    }

    static async getUpgradableRoles(player: GuildMember): Promise<Array<UpgradableRolesType>> {
        const allGroupedRoles = await RoleGroupItem.find();
        const playerRoles = player.roles.cache.map(role => role);

        const upgradableRoles: Array<UpgradableRolesType> = [];


        for(const groupedRole of allGroupedRoles) {
            // If the player has the role
            if (playerRoles.map(role => role.id).includes(groupedRole.idrole)) {
                // Get all roles within the same group
                const currentGroup = allGroupedRoles.filter(role => role.idgroup === groupedRole.idgroup);
                const higherRankedRoles = currentGroup.filter(role => role.rank > groupedRole.rank);

                // If there are higher ranked roles
                if (higherRankedRoles.length > 0) {

                    // Get the closest higher ranked role
                    higherRankedRoles.sort((a, b) => a.rank - b.rank)
                    const closestHigherRankedRole = higherRankedRoles[0];

                    // Get the original role
                    const originalRole = playerRoles.find(role => role.id === groupedRole.idrole)!;

                    // Add it to the list of upgradable roles
                    upgradableRoles.push({
                        "originalRole": originalRole,
                        "newRole": player.guild.roles.cache.get(closestHigherRankedRole.idrole)!
                    });
                }
            }
        }

        return upgradableRoles;
    }
}