import { EmbedBuilder } from "discord.js";
import { SuccessEmbedDto } from "./dto/success-embed.dto";
import { ExceptionEmbedDto } from "./dto/exception-embed.dto";

export class EmbedService {

    static buildSuccess({title, description}: SuccessEmbedDto) {
        return new EmbedBuilder()
            .setTitle(title)
            .setDescription(description || null)
            .setColor([0, 0xFF, 0])
            .setTimestamp()
    }


    static buildException({message, fix}: ExceptionEmbedDto) {
        const embed = new EmbedBuilder()
            .setTitle("/!\\ Une erreur est survenue /!\\") 
            .addFields([{
                "name": "Description",
                "value": message
            }])
            .setColor([0xFF, 0, 0])

        if (fix) {
            embed.addFields([{
                "name": "Possible explication",
                "value": fix
            }])
        }
        
        return embed;
    }
}