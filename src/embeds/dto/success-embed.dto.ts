export interface SuccessEmbedDto {
    title: string;
    description?: string;
}