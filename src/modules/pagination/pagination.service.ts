import { CreatePaginationDto } from "./dto/create-pagination.dto";
import { ActionRowBuilder, ButtonBuilder, ButtonInteraction, ButtonStyle } from "discord.js";
import { ButtonComponent, Discord } from "discordx";
import { BotException } from "../../errors/bot.exception";
import { PaginationItem, PaginationOptions } from "./interfaces/pagination.interface";

const START_BUTTON_ID = "pagination-start";
const PREVIOUS_BUTTON_ID = "pagination-previous";
const NEXT_BUTTON_ID = "pagination-next";
const END_BUTTON_ID = "pagination-end";

@Discord()
export class PaginationService {

    private static paginations: Map<string, PaginationOptions> = new Map();

    public static async addPagination({messageId, pagination, ttl = 60 * 60 * 24}: CreatePaginationDto): Promise<PaginationItem> {
        this.paginations.set(messageId, pagination);
        setTimeout(() => {
            this.paginations.delete(messageId);
        }, ttl * 1000);

        const firstPage = await this.getPage(pagination, 0)

        const buttons = new ActionRowBuilder<ButtonBuilder>()
            .addComponents(this.getButtons(messageId, pagination))

        const otherComponents = firstPage.components ?? [];

        return {
            ...firstPage,
            components: [buttons, ...otherComponents]
        }
    }

    public static async getPage(pagination: PaginationOptions, index: number) {
        return pagination.pages ? pagination.pages[index] : await pagination.pageResolver(index);
    }

    public static getPagination(messageId: string): PaginationOptions | undefined {
        return this.paginations.get(messageId);
    }

    private static getButtons(messageId: string, pagination: PaginationOptions) {
        const maxPage = pagination.pages ? pagination.pages.length : pagination.maxPage
        
        const buttons = [
            new ButtonBuilder()
                .setCustomId(`${START_BUTTON_ID}/${messageId}`)
                .setLabel("Début")
                .setEmoji("⏮")
                .setStyle(ButtonStyle.Primary),
            new ButtonBuilder()
                .setCustomId(`${PREVIOUS_BUTTON_ID}/${messageId}`)
                .setLabel("Précédent")
                .setStyle(ButtonStyle.Primary)
                .setEmoji("◀"),
            new ButtonBuilder()
                .setCustomId(`page-number-button`)
                .setLabel(`Page ${pagination.currentPageIndex + 1}/${maxPage}`)
                .setStyle(ButtonStyle.Success)
                .setDisabled(true),
            new ButtonBuilder()
                .setCustomId(`${NEXT_BUTTON_ID}/${messageId}`)
                .setLabel("Suivant")
                .setStyle(ButtonStyle.Primary)
                .setEmoji("▶"),
            new ButtonBuilder()
                .setCustomId(`${END_BUTTON_ID}/${messageId}`)
                .setLabel("Fin")
                .setStyle(ButtonStyle.Primary)
                .setEmoji("⏭")
        ];

        if (pagination.currentPageIndex === 0) {
            buttons[0].setDisabled(true);
            buttons[1].setDisabled(true);
        }

        if (pagination.currentPageIndex === maxPage - 1) {
            buttons[3].setDisabled(true);
            buttons[4].setDisabled(true);
        }

        return buttons;
    }

    private async updateMessage(messageId: string, interaction: ButtonInteraction, pagination: PaginationOptions) {
        const currentPage = await (pagination.pageResolver
            ? pagination.pageResolver(pagination.currentPageIndex) :
            pagination.pages[pagination.currentPageIndex]);

        // new buttons row
        const newButtons = new ActionRowBuilder<ButtonBuilder>()
            .addComponents(PaginationService.getButtons(messageId, pagination))

        // Isolate other components
        let components = interaction.message.components;
        for(let i = 0 ; i < components.length ; i++) {
            const current_component = components[i];
            const buttons = current_component.components
            // If the row is the pagination row, replace it with the new
            if (buttons[0].customId?.includes(START_BUTTON_ID) &&
                buttons[1].customId?.includes(PREVIOUS_BUTTON_ID) &&
                buttons[3].customId?.includes(NEXT_BUTTON_ID) &&
                buttons[4].customId?.includes(END_BUTTON_ID)
            ) {
                components[i] = newButtons as any; // May not work as expected
                break;
            }
        }

        interaction.update({
            ...currentPage,
            "components": components
        })
        .catch(() => {throw new BotException("Impossible de mettre à jour la pagination.")});
    }

    // ---------------------------------

    @ButtonComponent({"id": new RegExp(`${START_BUTTON_ID}/\\d+`, "i")})
    public async startButton(interaction: ButtonInteraction) {
        const messageId = interaction.customId.split("/")[1];

        const pagination = PaginationService.getPagination(messageId);
        if (!pagination) {
            interaction.update({
                "components": [],
            });
            return
        }
        pagination.currentPageIndex = 0;
        this.updateMessage(messageId, interaction, pagination);
    }

    @ButtonComponent({"id": new RegExp(`${PREVIOUS_BUTTON_ID}/\\d+`, "i")})
    public async previousButton(interaction: ButtonInteraction) {
        const messageId = interaction.customId.split("/")[1];

        const pagination = PaginationService.getPagination(messageId);
        if (! pagination) return;

        pagination.currentPageIndex--;
        this.updateMessage(messageId, interaction, pagination);
    }

    @ButtonComponent({"id": new RegExp(`${NEXT_BUTTON_ID}/\\d+`, "i")})
    public async nextButton(interaction: ButtonInteraction) {
        const messageId = interaction.customId.split("/")[1];

        const pagination = PaginationService.getPagination(messageId);
        if (! pagination) return;

        pagination.currentPageIndex++;
        this.updateMessage(messageId, interaction, pagination);
    }

    @ButtonComponent({"id": new RegExp(`${END_BUTTON_ID}/\\d+`, "i")})
    public async endButton(interaction: ButtonInteraction) {
        const messageId = interaction.customId.split("/")[1];

        const pagination = PaginationService.getPagination(messageId);
        if (! pagination) return;

        const maxPage = pagination.pages ? pagination.pages.length : pagination.maxPage;
        pagination.currentPageIndex = maxPage - 1;
        this.updateMessage(messageId, interaction, pagination);
    }
}