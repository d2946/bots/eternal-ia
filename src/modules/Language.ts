import { ApplicationCommandOptionType } from "discord.js";
import { ParameterDecoratorEx, SlashChoice, SlashOption } from "discordx";
import { z } from "zod";

type Languages = "fr" | "en" | "es";

const zLanguage = () => z.union([z.literal("fr"), z.literal("en"), z.literal("es")]);

/**
 * The function return type is any: I struggled for hours on how to do it better, but the other solutions are too ugly to be used
 * @param name 
 * @param description 
 * @returns 
 */
function getLangParameters(name: string, description: string = "Langue à utiliser"): any {
    return {
        "name": name,
        "type": "STRING",
        "required": false,
        "autocomplete": false,
        "description": description,
        "choices": [
            {
                "name": "Français",
                "value": "fr"
            },
            {
                "name": "English",
                "value": "en"
            },
            {
                "name": "Español",
                "value": "es"
            },
        ]
    }
}

/**
 * This function applies the 3 language choices and applies the option at the end
 * @param name 
 * @param required 
 */
function LangParameters(name: string, required: boolean): ParameterDecoratorEx {
    return (target: any, propertyKey: string, parameterIndex: number) => {
        SlashOption({
            "name": name as any, // TODO fix this
            "description": "Langue à utiliser",
            required,
            "type": ApplicationCommandOptionType.String
        })(target, propertyKey, parameterIndex);
        SlashChoice({
            "name": "Español",
            "value": "es"
        })(target, propertyKey, parameterIndex);
        SlashChoice({
            "name": "English",
            "value": "en"
        })(target, propertyKey, parameterIndex);
        SlashChoice({
            "name": "Français",
            "value": "fr"
        })(target, propertyKey, parameterIndex);
    }
}

export {
    LangParameters,
    getLangParameters,
    zLanguage
}
export type {
    Languages
}