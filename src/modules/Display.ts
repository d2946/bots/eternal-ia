interface BuildTableParamsType {
    title: string,
    headers: Array<string>,
    data: Array<Array<string>>
}

const MAX_CHARACTERS_PER_PAGE = 2000;

export function buildTable({title, headers, data}: BuildTableParamsType): Array<string> {

    const pages: Array<string> = [];

    const lengths = getLengths(headers, data);
    let rows = data.map(row => buildRow(row, lengths))

    if (rows.length === 0) {
        return ["```\n" +
            getPageUpperHeader(title) + "\n" +
            buildTableHeader(headers, lengths) + "\n\n" +
            "Aucune donnée trouvée\n" +
            // buildRow(Array(lengths.length).fill(""), lengths) + "\n" +
            // getFooter(1, 1) +
        "```"]
    }

    const rowLength = getRowLength(lengths);

    const linesPerPage = Math.floor((MAX_CHARACTERS_PER_PAGE - 10 - title.length) / rowLength) // 10 is a margin
        - 2 // For title and newline
        - 3 // For table header
        - 1; // For footer

    for(let i = 0 ; i < rows.length ; i += linesPerPage) {
        const currentPageBody = rows.slice(i, i + linesPerPage).join("\n");

        const currentPage = "```\n" +
            getPageUpperHeader(title) + "\n" +
            buildTableHeader(headers, lengths) + "\n" +
            currentPageBody + "\n" +
            getLine(getRowLength(lengths)) + "\n" +
            // getFooter(Math.floor(i / linesPerPage) + 1, Math.ceil(rows.length / linesPerPage)) +
        "```";

        pages.push(currentPage);
    }

    return pages;
}

function getRowLength(lengths: Array<number>) {
    return lengths.reduce((acc, elem) => acc + elem, 0)
        + 3 * (lengths.length - 1) // " | "
        + 2 * 2 // " |" + "| "
}

function getLine(length: number, insideTable: boolean = false) {
    const beginning = insideTable ? "| " : "--";
    const end = insideTable ? " |" : "--";
    return beginning +
        "-".repeat(length - beginning.length - end.length)
        + end;
}

function buildRow(row: Array<string>, lengths: Array<number>) {
    return "| "
        + row
            .map((row, indexRow) => `${row.padEnd(lengths[indexRow], " ")}`)
            .join(" | ")
        + " |"
}

function buildTableHeader(headers: Array<string>, lengths: Array<number>) {
    const rowLength = getRowLength(lengths);
    return getLine(rowLength)
        + "\n| "
        + headers
            .map((header, indexHeader) => `${header.padEnd(lengths[indexHeader], " ")}`)
            .join(" | ")
        + " |\n"
        + getLine(rowLength, true)
}

function getLengths(headers: Array<string>, data: Array<Array<string>>) {
    const lengths = headers.map(elem => elem.length)

    data.forEach(ligne => {
        ligne.forEach((row, indexRow) => {
            lengths[indexRow] = Math.max(row.length, lengths[indexRow]);
        })
    })

    return lengths;
}

function getPageUpperHeader(title: string) {
    return `~~~~ ${title} ~~~~\n\n`;
}

function getFooter(pageNumber: number, totalPages: number) {
    return `Page ${pageNumber} / ${totalPages}`
}

function getAllHeaders(headers: Array<string>, lengths: Array<number>): string {
    const headerTexts = headers
        .map((elem, index) => elem.padEnd(lengths[index]))
        .join(" | ");

    let header = `| ${headerTexts} |\n`
    header += `| ${"-".repeat(lengths.reduce((acc, elem) => acc + elem) + lengths.length * 3 - 3)} |`;

    return header;
}