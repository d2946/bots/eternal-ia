import { AxiosProps } from "@hamm4all/shared"
import axios from "axios"
import { ENV } from "../../Environment"

export const H4A_BASE_URL = ENV.APP_ENV === "DEV" ? "http://localhost:8080" : "https://hamm4all.net"
const axiosInstance = axios.create({
    "baseURL": H4A_BASE_URL,
})

export default async function callH4AAPI<Response>(props: Readonly<AxiosProps<{}, {}> & {isFile?: boolean}>) {
    const request = axiosInstance.request<Response>({
        method: props.method,
        url: props.url,
        headers: props.headers,
        params: props.query,
        responseType: props.isFile ? "arraybuffer" : "json",
    })

    return request.then(response => response.data)
}