import { H4A_API, Item } from "@hamm4all/shared";
import { ApplicationCommandOptionChoiceData } from "discord.js";
import { normalMatch } from "../../../match/matchers";
import { applyAPICaching } from "../hamm4all-cache";

export default class H4AItems {
    static async getItems() {
        return applyAPICaching({
            "endpoint": H4A_API.v1.items.list,
            "params": {},
            "query": {},
            "timeToLive": 60 * 60
        })
    }

    static async getItemsByCondition(condition: (item: Item) => boolean) {
        const items = await this.getItems()
        return items.filter(condition);
    }

    static async getItemByCondition(condition: (item: Item) => boolean) {
        const items = await this.getItems()
        return items.find(condition);
    }

    static async getItemByGameIDAndInGameID(gameID: number, itemInGameID: number) {
        return this.getItemByCondition((item) => item.in_game_id === itemInGameID && item.game.id === gameID);
    }

    static async autoCompleteItems(input: string) {
        const items = await H4AItems.getItems();

        const mapped: Array<ApplicationCommandOptionChoiceData> = items
            .flatMap(item => {
                const languages = Object.keys(item.name);
                return languages.map(lang => `[${lang.toUpperCase()}] ${item.name[lang]}`)
            })
            .filter(item => normalMatch(item, input))
            .map(item => ({
                "name": item,
                "value": item
            }))
        return mapped.slice(0, 24);
    }
}