import { Family, H4A_API } from "@hamm4all/shared";
import { applyAPICaching } from "../hamm4all-cache";
import { normalMatch } from "../../../match/matchers";

export default class H4AFamilies {
    static async getFamilies() {
        return applyAPICaching({
            "endpoint": H4A_API.v1.families.list,
            "params": {},
            "query": {},
            "timeToLive": 60 * 60 * 24 * 30
        })
    }

    static async getFamiliesByCondition(condition: (family: Family) => boolean) {
        const families = await this.getFamilies()
        return families.filter(condition);
    }

    static async getFamilyByName(name: string) {
        const families = await this.getFamilies()
        return families.find(family => {
            const names = [family.name.fr, family.name.en, family.name.es]
            return names.includes(name)
        })
    }

    static async autoCompleteFamilies(input: string) {
        const families = await this.getFamilies();
        const mapped = families
            .flatMap(family => [`[FR] ${family.name.fr}`, `[EN] ${family.name.en}`, `[ES] ${family.name.es}`])
            .filter(family => normalMatch(family, input))
            .map(family => ({
                "name": family,
                "value": family
            }))

        return mapped.slice(0, 24);
    }
}