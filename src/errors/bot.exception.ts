export class BotException extends Error {
    public readonly fix: string | undefined;

    constructor(message: string, fix?: string) {
        super(message);
        this.fix = fix;
    }
}