import BotError from "./BotError";

export class GameNotFoundError extends BotError {
    constructor(gameName: string) {
        super(`La contrée \`${gameName}\` n'existe pas, ou n'a pas été trouvée.`);
    }
}