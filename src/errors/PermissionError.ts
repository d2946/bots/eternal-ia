import { getPermissionName, Grade } from "../permissions/Permissions";
import BotError from "./BotError";

export default class PermissionError extends BotError {
    constructor(currentRole: Grade, requiredRole: Grade) {
        super(`**Vous n'avez pas accès à cette commande**.\nVotre grade : **${getPermissionName(currentRole)}**\nGrade requis : **${getPermissionName(requiredRole)}**`)
    }
}
