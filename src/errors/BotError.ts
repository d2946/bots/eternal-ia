
class BotError extends Error {

    debugInfo: string | undefined

    constructor(message: string, debugInfo?: string) {
        super(`${message}`)
        this.debugInfo = debugInfo;
    }
}

export default BotError