import { Client } from "discord.js";
import { logger } from "./Log";
import cron from "node-cron";
import { ENV } from "./Environment";
import { NDJPing } from "./database/models/NDJPing";
import { QuizStart } from "./commands/quiz/QuizStart";

function initCrons(client: Client) {
    cron.schedule(ENV.NDJ_REFRESH_CRON, async () => {
        await NDJPing.resetNDJ()
        logger.info("NDJ ping refreshed")
    })

    cron.schedule("0 0 * * *", () => {
        const time = Math.random() * 23;
        setTimeout(() => {
            QuizStart.startHappyHour(client)
            logger.info("Happy hour starts!")
        }, time * 60 * 60 * 1000)
    })

    logger.verbose("Crons set")
}

export {
    initCrons
}