FROM node:22-alpine

WORKDIR /app

RUN apk add --update --no-cache \
    make \
    g++ \
    jpeg-dev \
    cairo-dev \
    giflib-dev \
    pango-dev \
    libtool \
    autoconf \
    automake

COPY .yarnrc.yml .
COPY ./package.json .

RUN yarn set version stable \
    && yarn install

COPY . .

RUN yarn compile &&
    yarn cache clean &&
    chmod u+x ./container/create-env.sh

ENTRYPOINT ["./container/create-env.sh"]

CMD [ "yarn", "start" ]
